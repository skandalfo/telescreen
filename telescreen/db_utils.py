from aiocontextvars import ContextVar
from asyncpg.exceptions import SerializationError, PostgresConnectionError

from telescreen.log_utils import F

_current_transaction = ContextVar('current_transaction', default=None)


class TransactionContext(object):
    def __init__(self, server_object):
        self.server_object = server_object
        self.on_commit_coroutines = []

    @staticmethod
    def get_active():
        return _current_transaction.get()

    @staticmethod
    def set_active(ctx):
        _current_transaction.set(ctx)

    def run_after_commit(self, coroutine):
        self.on_commit_coroutines.append(coroutine)


def serializable_transaction(
        num_retries=10,
        connection_error_delay=10,
        force_new=False):
    def decorate(fn):
        async def wrapper(self, *args, **kwargs):
            prev_ctx = TransactionContext.get_active()
            if prev_ctx and not force_new:
                # We'll just reuse the existing context, so just call the decorated
                # function without any further ado. No retries apply as we are not
                # creating a new context here; the top transaction will retry if
                # needed.
                return await fn(self, *args, **kwargs)

            retries = num_retries
            on_commit_coroutines = []
            while True:
                # Stack a new transaction context.
                TransactionContext.set_active(TransactionContext(self))
                try:
                    async with self.server.db.transaction(isolation='serializable', reuse=False):
                        result = await fn(self, *args, **kwargs)

                    # At this point we have committed the transaction, so sample
                    # all on commit coroutines.
                    on_commit_coroutines = TransactionContext.get_active().on_commit_coroutines

                    # Return the result. The on commit coroutines will be run in the finally
                    # block after the current context gets popped out.
                    return result

                except SerializationError:
                    self.logger.warning(
                        F('Transaction serialization failure, {#E:{}#} retries left.', retries),
                        exc_info=True)
                    retries -= 1
                    if retries <= 0:
                        raise
                except PostgresConnectionError:
                    self.logger.warning(
                        F('Database connection failure, {#E:{}#} retries left.', retries),
                        exc_info=True)
                    retries -= 1
                    if retries <= 0:
                        raise
                    await self.sleep(connection_error_delay)
                finally:
                    # Reset the current transaction context to its previous
                    # value.
                    TransactionContext.set_active(prev_ctx)

                    # Run any on commit coroutines.
                    await self.gather(*on_commit_coroutines)

        return wrapper

    return decorate

import datetime
import pkg_resources
import signal

from aiohttp import web
import aiohttp_debugtoolbar

import asyncio
import logging
from telescreen.log_utils import F, colorize


LOG_INFO = -1
STDOUT = -2
DEVNULL = -3


def kill_server_on_exception(fn):
    async def wrapper(self, *args, **kwargs):
        try:
            return await fn(self, *args, **kwargs)
        except asyncio.CancelledError:
            return
        except Exception:
            self.logger.exception(F('Uncaught exception; finishing server.'))
            await self.shield(self.server.stop())

    return wrapper


def logging_line_processor(logger):
    async def log_to_logger(text):
        logger(text)

    return log_to_logger


def line_processor_loop(line_processor):
    async def line_processor_loop(stream):
        while True:
            line = await stream.readline()
            if len(line) == 0:
                return
            text = line.decode(errors='replace').rstrip('\n')
            await line_processor(text)

    return line_processor_loop


def copy_stream(destination, buffer_size=1024**2, max_size=None):
    async def copy_stream(stream):
        bytes_copied = 0
        while max_size is None or bytes_copied < max_size:
            size = (
                buffer_size
                if max_size is None
                else min(max_size - bytes_copied, buffer_size)
            )
            data = await stream.read(size)
            if len(data) == 0:
                break
            await destination.write(data)

    return copy_stream


class Subprocess(object):
    def __init__(self, server_object, process, stdin_future,
                 stdout_future, stderr_future, kill_delay, terminate_signals):
        self.server_object = server_object
        self.logger = server_object.logger
        self.process = process
        self.stdin_future = stdin_future
        self.stdout_future = stdout_future
        self.stderr_future = stderr_future
        self.kill_delay = kill_delay
        self.terminate_signals = terminate_signals
        self._exit_code_future = server_object.spawn(self._exit_code())
        
    async def terminate(self):
        self.logger.info(F('Terminating process...'))
        final_exc = ValueError('Empty terminate_signals specified.')
        
        for sig in self.terminate_signals:
            self.logger.info(F('Sending {#e:{}#} signal.', sig))
            
            # Check if the process already finished.
            exit_code = self.process.returncode
            if exit_code is not None:
                return exit_code
            
            try:
                self.process.send_signal(sig)
            except ProcessLookupError:
                # The process doesn't exist anymore. Race?
                return self.process.returncode
            
            try:
                exit_code = await self.server_object.wait_for(self.process.wait(), self.kill_delay)
                self.logger.info(F('Process stopped with error code {#e:{}#}', exit_code))
                return exit_code
            except asyncio.TimeoutError as exc:
                final_exc = exc
                
        raise final_exc
    
    async def exit_code(self):
        return await self._exit_code_future
    
    async def _exit_code(self):
        try:
            result = await self.server_object.gather(
                self.process.wait(),
                self.stdin_future,
                self.stdout_future,
                self.stderr_future)
        
            # Get the exit code of the process.
            exit_code = result[0]
            if exit_code:
                self.logger.error(
                    F('Child process exited with error code {#e:{}#}', exit_code))
            return exit_code
        finally:
            await self.terminate()
            await self.server_object.cancel(self.stdin_future)
            await self.server_object.cancel(self.stdout_future)
            await self.server_object.cancel(self.stderr_future)


class ServerObject(object):
    def __init__(self, name, server):
        self.server = server
        self.name = name
        self.logger = self.make_logger()
        self._lock = asyncio.Lock()
        self._running = False
        self._starting = False
        self._stopping = False
        self._running_cond = asyncio.Condition(
            self._lock)
        self.logging_name = colorize('{#t:{}#}.{#T:{}#}').format(
            self.__class__.__name__,
            self.name)

        self.logger.info(F('{} being configured...', self.logging_name))
        self.subapp = None

    def _make_app(self):
        app = web.Application(
            handler_args=dict(
                logger=self.logger, access_log=self.make_logger('access'),
                access_log_format=colorize(
                    '{#e:%a#} {#*I:%t#} {#*_r:"%r"#} {#E:%s#} {#w:%b#} {#R:%{Referer}i#} {#t:%{User-Agent}i#}')))
        app['app_name'] = '{}/{}'.format(self.__class__.__name__, self.name)

        # Enable the debugging toolbar if requested.
        if self.server.enable_debug_toolbar:
            aiohttp_debugtoolbar.setup(
                app, hosts=self.server.debug_toolbar_allowed_hosts,
                intercept_redirects=False)
        return app

    def _list_packages(self, group):
        self.logger.info(F('Finding {#t:{}#} packages...', group))
        packages = []
        for entry_point in pkg_resources.iter_entry_points(
                group):
            set_name = entry_point.name
            package_instance = entry_point.load()
            self.logger.info(
                F(
                    'Using {#t:{}#} set {#T:{}#} within package {#t:{}#}.',
                    group,
                    set_name,
                    package_instance.__name__))
            packages.append(package_instance)

        return packages

    def make_logger(self, *suffixes):
        elements = (self.__class__.__name__, self.name) + suffixes
        return logging.getLogger('.'.join(elements))

    async def start(self):
        try:
            async with self._lock:
                if not self._running:
                    self.logger.info(F('Starting {}...', self.logging_name))
                    self._starting = True
                    await self._start()
                    self._running = True
                    self._starting = False
                    self._running_cond.notify_all()
                    self.logger.info(F('{} started.', self.logging_name))
        except Exception:
            await self.stop()
            raise

    async def _start(self):
        pass

    async def run_if_started(self, method):
        async with self._lock:
            if self._running:
                await method()

    async def stop(self):
        async with self._lock:
            if self._running:
                self.logger.info(
                    F('Stopping {}...', self.logging_name))
                self._stopping = True
                self._running = False
                await self._stop()
                self._stopping = False
                self._running_cond.notify_all()
                self.logger.info(
                    F('{} stopped.', self.logging_name))

    async def _stop(self):
        pass

    async def run(self):
        await self.start()

        async with self._running_cond:
            await self._running_cond.wait_for(lambda: not self._running)

        await self.stop()

    @kill_server_on_exception
    async def run_or_die(self, coro):
        return await coro

    async def cancel(self, task):
        task.cancel()
        while True:
            try:
                return await self.wait_for(task, datetime.timedelta(seconds=5))
            except asyncio.TimeoutError:
                self.logger.warning(F('Still waiting for cancellation...'))
            except asyncio.CancelledError:
                return

    def spawn(self, coro):
        return asyncio.ensure_future(
            self.run_or_die(coro))

    async def spawn_subprocess(self, *args, stdin=DEVNULL, stdout=DEVNULL, stderr=LOG_INFO, kill_delay=datetime.timedelta(seconds=30), terminate_signals = None):
        terminate_signals = terminate_signals or [signal.SIGTERM, signal.SIGKILL]
        
        self.logger.info(
            F('Starting child process with commandline: {#E:{}#}', ' '.join(args)))

        async def no_op(stream):
            pass

        def make_in_handler(value):
            if value == DEVNULL:
                return asyncio.subprocess.DEVNULL, no_op
            else:
                return asyncio.subprocess.PIPE, value

        def make_out_handler(value):
            if value == DEVNULL:
                return asyncio.subprocess.DEVNULL, no_op
            elif value == STDOUT:
                return asyncio.subprocess.STDOUT, no_op
            elif value == LOG_INFO:
                return asyncio.subprocess.PIPE, line_processor_loop(
                    logging_line_processor(self.logger.info))
            else:
                return asyncio.subprocess.PIPE, value

        stdin_strategy, stdin_handler = make_in_handler(stdin)
        stdout_strategy, stdout_handler = make_out_handler(stdout)
        stderr_strategy, stderr_handler = make_out_handler(stderr)

        proc = await asyncio.create_subprocess_exec(*args, stdin=stdin_strategy, stdout=stdout_strategy, stderr=stderr_strategy)
        stdin_future = self.spawn(stdin_handler(proc.stdin))
        stdout_future = self.spawn(stdout_handler(proc.stdout))
        stderr_future = self.spawn(stderr_handler(proc.stderr))

        self.logger.info(
            F('Started child process with PID: {#E:{}#}', proc.pid))

        return Subprocess(self, proc, stdin_future, stdout_future, stderr_future, kill_delay, terminate_signals)

    async def sleep(self, delay):
        return await asyncio.sleep(delay.total_seconds())

    async def await_later(self, delay, coro):
        if delay > datetime.timedelta(0):
            await self.sleep(delay)
        return await coro

    async def wait_for(self, fut, timeout):
        return await asyncio.wait_for(fut, timeout.total_seconds() if timeout is not None else None)

    async def shield(self, arg):
        return await asyncio.shield(arg)

    async def gather(self, *coros_or_futures, return_exceptions=False):
        return await asyncio.gather(*coros_or_futures, return_exceptions=return_exceptions)

import aiohttp_jinja2
import datetime
import signal
import socket
import ssl

from aiohttp import web
import gino

import aiohttp_security
from configparser import _UNSET
import fnmatch
from telescreen import resource_utils, config_utils, schema, time_utils,\
    web_utils
from telescreen.auth_manager import AuthManager
from telescreen.log_utils import F
from telescreen.serverobject import ServerObject
from telescreen.signals import Signal


DEFAULT_HTTP_PORT = 8282
DEFAULT_HTTPS_PORT = None

try:
    import systemd
    import systemd.daemon

    if not systemd.daemon.booted():
        systemd = None
    else:
        sockets = [-fd for fd in systemd.daemon.listen_fds()]
        DEFAULT_HTTP_PORT, DEFAULT_HTTPS_PORT = (sockets + [
            DEFAULT_HTTP_PORT, DEFAULT_HTTPS_PORT][len(sockets):])[:2]
except ImportError:
    systemd = None


class NavigationItem(object):
    def __init__(self, css_class, text, link):
        self.css_class = css_class
        self.text = text
        self.link = link


class TeleScreenServer(ServerObject):
    resources = web_utils.WebResources()

    def __init__(self, config):
        super().__init__(config['name'], self)
        self.signals = {}
        self.port = config.getint('port', DEFAULT_HTTP_PORT)
        self.tls_port = config.getint('tls_port', DEFAULT_HTTPS_PORT)
        self.certificate_chain = config.get('certificate_chain')
        self.private_key = config.get('private_key')
        self.auth_manager = AuthManager(config, self)
        self.resource_manager = resource_utils.ResourceManager(config, self)
        self.template_loader = resource_utils.TemplateLoader(config, self)
        self.context_processors = [self._navigation_items]

        self.static_dirs = config_utils.resolve_mapping(
            config.getmapping('static_dirs', []))

        self.db_connection_string = config.get('db_connection_string', _UNSET)

        self.enable_debug_toolbar = config.getboolean(
            'enable_debug_toolbar', False)
        self.debug_toolbar_allowed_hosts = config.getlist(
            'debug_toolbar_allowed_hosts', [])

        self.app = self._make_app()

        self.site = None
        self.tls_site = None

        self.signal_scan_period = config.getduration(
            'signal_scan_period', time_utils.duration('30s'))

        main_config = config.parser
        self.cameras = self.construct(main_config,
                                      'telescreen.camera.Camera')
        self.receivers = self.construct(main_config,
                                        'telescreen.receiver.Receiver')
        self.notifications = self.construct(
            main_config, 'telescreen.notification.Notification')
        self.events = self.construct(main_config,
                                     'telescreen.event.Event')

    def construct(self, config, group_name):
        filters = {'run_in_server': self.name}
        results = config.construct(group_name, self, filters=filters)

        # Check for subapps.
        for name, result in results.items():
            subapp = result.subapp
            if subapp is not None:
                subapp['parent_app'] = self.app
                self.app.add_subapp('/{}/{}/'.format(group_name, name), subapp)
                self.logger.info(
                    F('Added object subapp at {#R:/{}/{}/#}.',
                      group_name, name))

        return results

    async def _navigation_items(self, request):
        identity = await aiohttp_security.authorized_userid(request)

        items = [
            NavigationItem(
                resource.navbar_class or '',
                resource.navbar_name,
                resource.name or None)
            for resource in self.resources + self.auth_manager.resources
            if resource.navbar_name and (identity or resource.public)
        ]
        if identity:
            items.append(
                NavigationItem(
                    'info',
                    'Logged in as {}'.format(identity),
                    None)
            )

        for item in items:
            if item.link:
                resource = request.app.router[item.link]
                url = resource.url_for()

                # Mark the item as selected if the request matches its path.
                if request.path == url.path:
                    item.css_class = ' '.join([item.css_class, 'selected'])

                # Replace the resource name with a relative route.
                item.link = url

        return dict(identity=identity, navigation_items=items)

    def _make_app(self):
        app = super()._make_app()

        # Set up session-based security.
        self.auth_manager.setup(
            app, force_https=(self.port and self.tls_port))

        # Configure the resources handler; make the resources public.
        self.resource_manager.setup(app)
        self.auth_manager.public_resources.add(app.router['resources'])

        # Set up jinja2 templates.
        aiohttp_jinja2.setup(
            app,
            loader=self.template_loader,
            context_processors=self.context_processors)

        # Configure handlers.
        self.resources.add(
            '/ping3',
            self._handle_ping,
            name='ping3',
            redirect_to_login=False)
        self.resources.setup(self, app, self.auth_manager)

        # Add static directories.
        for path, prefixes in self.static_dirs.items():
            for prefix in prefixes:
                self.auth_manager.public_resources.add(
                    app.router.add_static(prefix, path))

        return app

    @resources.register('/', name='root')
    @aiohttp_jinja2.template('base.jinja2')
    async def _handle_root(self, unused_request):
        return {}

    @resources.register('/ping', name='ping', redirect_to_login=False)
    async def _handle_ping(self, request):
        return await web_utils.serve_text(request, 'pong', max_age=datetime.timedelta(seconds=0))

    @resources.register('/streams', name='streams', navbar_name='Streams')
    @aiohttp_jinja2.template('base.jinja2')
    async def _handle_streams(self, unused_request):
        return {}

    @resources.register('/timeline', name='timeline', navbar_name='Timeline')
    @aiohttp_jinja2.template('timeline.jinja2')
    async def _handle_timeline(self, unused_request):
        return {}

    @resources.register(
        '/timeline.json',
        name='timeline.json',
        redirect_to_login=False)
    async def _handle_timeline_json(self, request):
        start = (
            datetime.datetime.fromtimestamp(
                float(request.query.getone('start')),
                tz=datetime.timezone.utc)
            if 'start' in request.query else datetime.datetime.min)
        end = datetime.datetime.now(tz=datetime.timezone.utc)

        async def process_camera(camera):
            return (
                camera.name,
                await camera.get_recorded_intervals(start, end)
            )

        async def process_event(event):
            return (
                event.name,
                await event.get_event_history(start, end)
            )

        cameras = [process_camera(camera) for camera in self.cameras.values()]
        events = [process_event(event) for event in self.events.values()]

        cameras, events = await self.gather(
            self.gather(*cameras),
            self.gather(*events)
        )

        cameras = {name: intervals for name, intervals in cameras}
        events = {
            event_name: event_instances for event_name,
            event_instances in events
        }

        continue_at = end - 5 * max(camera.streaming_delay
                                    for camera in self.cameras.values())

        return await web_utils.serve_json(request, {
            'up_to': time_utils.time_to_json(end),
            'continue_at': time_utils.time_to_json(continue_at),
            'cameras': cameras,
            'events': events
        })

    def _get_by_pattern(self, dictionary, pattern):
        result = [value
                  for key, value in dictionary.items()
                  if fnmatch.fnmatchcase(key, pattern)]

        if not result:
            self.logger.warning(
                F('Pattern {#r:{}#} did not match any object.', pattern))

        return result

    async def _start_group(self, group):
        await self.gather(*[component.start() for component in group.values()])

    async def _stop_group(self, group):
        await self.gather(*[component.stop() for component in group.values()])

    def add_signal(self, name):
        if name in self.signals:
            return self.signals[name]
        else:
            signal = Signal(name, self)
            self.signals[name] = signal
            return signal

    def get_signals(self, pattern):
        return self._get_by_pattern(self.signals, pattern)

    def get_cameras(self, pattern):
        return self._get_by_pattern(self.cameras, pattern)

    def get_notifications(self, pattern):
        return self._get_by_pattern(self.notifications, pattern)

    def get_events(self, pattern):
        return self._get_by_pattern(self.events, pattern)

    async def _systemd_notify(self, state):
        if systemd:
            systemd.daemon.notify(state)

    async def _start(self):
        await super()._start()
        self.db = await gino.create_engine(self.db_connection_string)
        await schema.db.gino.create_all(bind=self.db)
        await self.auth_manager.start()
        await self.resource_manager.start()
        await self.template_loader.start()
        await self._start_group(self.events)
        await self._start_group(self.signals)
        await self._start_group(self.cameras)
        await self._start_group(self.receivers)
        await self._start_group(self.notifications)
        self.runner = web.AppRunner(self.app)
        await self.runner.setup()

        def make_site(port, **kwargs):
            if port > 0:
                self.logger.info(
                    F('Listening on port {#E:{}#}.', port))
                return web.TCPSite(self.runner, port=port, **kwargs)
            else:
                self.logger.info(
                    F('Listening on file descriptor {#E:{}#}.', -port))
                return web.SockSite(
                    self.runner, sock=socket.fromfd(-port, socket.AF_INET, socket.SOCK_STREAM),
                    **kwargs)

        if self.port:
            self.site = make_site(self.port)
            await self.site.start()
        if self.tls_port:
            ssl_context = ssl.SSLContext()
            ssl_context.load_cert_chain(
                self.certificate_chain, self.private_key)
            self.tls_site = make_site(
                self.tls_port, ssl_context=ssl_context)
            await self.tls_site.start()

        await self._systemd_notify('READY=1')

    async def _stop(self):
        await self._systemd_notify('STOPPING=1')

        if self.tls_site:
            await self.tls_site.stop()
            self.tls_site = None
        if self.site:
            await self.site.stop()
            self.site = None
        await self.runner.cleanup()
        await self._stop_group(self.notifications)
        await self._stop_group(self.receivers)
        await self._stop_group(self.cameras)
        await self._stop_group(self.signals)
        await self._stop_group(self.events)
        await self.template_loader.stop()
        await self.resource_manager.stop()
        await self.auth_manager.stop()
        await self.db.close()
        await super()._stop()


@TeleScreenServer.resources.register(
    '/ping2', name='ping2', redirect_to_login=False, unbound=True)
async def ping2(request):
    return await web_utils.serve_text(request, 'pong', max_age=datetime.timedelta(seconds=0))

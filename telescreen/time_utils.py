import datetime
import email

_SUFFIXES = {
    's': 1,
    'm': 60,
    'h': 60 * 60,
    'd': 60 * 60 * 24,
    'w': 60 * 60 * 24 * 7,
    'M': 60 * 60 * 24 * 30,
    'q': 60 * 60 * 24 * 91,
    'y': 60 * 60 * 24 * 365,
}


def duration(text):
    if not text:
        raise ValueError('Empty duration')
    result = datetime.timedelta()

    for component in text.split():
        suffix = component[-1]
        if suffix in _SUFFIXES:
            multiplier = _SUFFIXES[suffix]
            component = component[:-1]
        else:
            multiplier = 1

        result += datetime.timedelta(seconds=multiplier * int(component))

    return result


# This prints a human readable string in the server's local TZ.
def time_to_human(when):
    return when.astimezone().strftime('%Y-%m-%d %H:%M:%S %z')


def time_to_filename(when):
    return when.astimezone().strftime('%Y%m%d_%H%M%S_%z')


def time_to_iso(when):
    return when.isoformat('T')


def iso_to_time(text):
    # Python doesn't support parsing back ISO datetimes. Sigh.
    tz_spec = ''

    if text.endswith('z') or text.endswith('Z'):
        text = text[:-1] + '+0000'
        tz_spec = '%z'
    elif (len(text) >= 6 and
          text[-6] in '+-' and
          text[-5:-3].isdigit() and
          text[-3] == ':' and
          text[-2:].isdigit()):
        text = text[:-3] + text[-2:]
        tz_spec = '%z'
    elif (len(text) >= 5 and
          text[-5] in '+-' and
          text[-4:].isdigit()):
        tz_spec = '%z'
    try:
        return datetime.datetime.strptime(
            text, '%Y-%m-%dT%H:%M:%S.%f' + tz_spec)
    except ValueError:
        # Try again, without the fractional seconds. Sigh, ugh.
        return datetime.datetime.strptime(
            text, '%Y-%m-%dT%H:%M:%S' + tz_spec)


def time_to_http(when):
    return email.utils.format_datetime(when, usegmt=True)


def http_to_time(text):
    return email.utils.parsedate_to_datetime(text)


def duration_to_srt(delta):
    dur = delta.total_seconds()
    millis = int(dur * 1000) % 1000
    secs = int(dur) % 60
    mins = int(dur) // 60 % 60
    hours = int(dur) // 3600 % 60

    return '{:02}:{:02}:{:02},{:03}'.format(hours, mins, secs, millis)


def time_to_json(when):
    return when.timestamp() if when else None

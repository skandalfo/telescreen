import aiosmtplib
import datetime
import email
import logging

from telescreen import time_utils, schema, periodic_task
from telescreen.db_utils import serializable_transaction, TransactionContext
from telescreen.log_utils import F
from telescreen.serverobject import ServerObject


class Notification(ServerObject):
    def __init__(self, config, server):
        super().__init__(config['name'], server)
        self.max_retries = config.getint('max_retries', 10)
        self.min_retry_time = config.getduration(
            'min_retry_time', time_utils.duration('2s'))
        self.max_retry_time = config.getduration(
            'max_retry_time', time_utils.duration('30m'))
        self.queue_scan_period = config.getduration(
            'queue_scan_period', time_utils.duration('1m'))
        self.retention_time = config.getduration('retention_time', None)
        self.garbage_collect_period = config.getduration(
            'garbage_collect_period', time_utils.duration('5m'))

        # Periodically process queued notifications.
        self._queue_processor = periodic_task.PeriodicTask(
            self.server,
            self.name + '_queue',
            self.queue_scan_period,
            self._process_pending_notifications)

        # GC periodic task.
        self._gc_task = periodic_task.PeriodicTask(
            self.server,
            self.name + '_gc',
            self.garbage_collect_period,
            self._garbage_collect_old_notifications)

    async def _start(self):
        await super()._start()
        await self._queue_processor.start()
        await self._gc_task.start()

    async def _stop(self):
        await self._gc_task.stop()
        await self._queue_processor.stop()
        await super()._stop()

    @serializable_transaction()
    async def _garbage_collect_old_notifications(self):
        if not self.retention_time:
            return

        # Calculate a protected interval depending on retention.
        now = datetime.datetime.now(tz=datetime.timezone.utc)
        retain_since = now - self.retention_time

        # Remove all the entries from the database.
        delete_query = schema.Notification.delete.where(
            (schema.Notification.name == self.name) & (
                not schema.Notification.pending)).where(
            schema.Notification.next_retry < retain_since)

        await self.server.db.status(delete_query)

    async def _process_pending_notifications(self):
        now = datetime.datetime.now(tz=datetime.timezone.utc)

        while True:
            next_delay = await self._process_one_pending_notification(now)
            if next_delay:
                return next_delay
            # Otherwise, keep going.

    @serializable_transaction()
    async def _process_one_pending_notification(self, now):
        pending = await self.server.db.first(
            schema.Notification.query.where(
                (schema.Notification.name == self.name) &
                (schema.Notification.pending))
            .order_by(schema.Notification.next_retry)
            .limit(1)
        )

        if not pending:
            # Nothing queued.
            return self.queue_scan_period

        if pending.next_retry > now:
            # Return the time until the first pending notification.
            return pending.next_retry - now

        # This one is due. Try to send it out.
        try:
            await self._notify(
                pending.source,
                pending.thread_id,
                pending.when,
                pending.message_id,
                pending.message)
        except Exception:
            if pending.retry_count >= self.max_retries:
                self.logger.exception(
                    F(
                        'Problem while trying to deliver {#T:{}#} notification with ID {#E:{}#}; no more retries left.',
                        pending.name,
                        pending.id))
                await pending.update(pending=False, failed=True).apply(bind=self.server.db)
                # Keep processing by returning a zero delay.
                return datetime.timedelta()
            else:
                next_retry = pending.next_retry + min(
                    self.max_retry_time, 2**pending.retry_count * self.min_retry_time)
                self.logger.exception(
                    F(
                        'Problem while trying to deliver {#T:{}#} notification with ID {#E:{}#}; next retry at {#E:{}#}.',
                        pending.name,
                        pending.id,
                        next_retry))
                await pending.update(retry_count=pending.retry_count + 1, next_retry=next_retry).apply(bind=self.server.db)
                # Keep processing by returning a zero delay.
                return datetime.timedelta()

        # Mark this one as done.
        await pending.update(pending=False).apply(bind=self.server.db)

        # Keep processing by returning a zero delay.
        return datetime.timedelta()

    @serializable_transaction()
    async def notify(self, source, thread_id, when, message_id, message):
        notification = await schema.Notification(
            name=self.name,
            pending=True, failed=False,
            next_retry=when, retry_count=0,
            when=when, source=source, thread_id=thread_id, message_id=message_id, message=message).create(bind=self.server.db)
        self.logger.info(
            F(
                'Notification {#T:{}#} with ID {#E:{}#} queued at {#E:{}#}.',
                notification.name,
                notification.id,
                notification.when))

        # Awaken the queue processor on commit.
        TransactionContext.get_active().run_after_commit(self._queue_processor.awaken())

    async def _notify(self, source, thread_id, when, message_id, message):
        pass


class LoggingNotification(Notification):
    def __init__(self, config, server):
        super().__init__(config, server)
        level = config.get('level', 'WARNING')
        self.level = getattr(logging, level)

    async def _notify(self, source, thread_id, when, message_id, message):
        await super()._notify(source, thread_id, when, message_id, message)
        self.logger.log(self.level,
                        F('{}: {}\n{}',
                          source,
                          message_id,
                          message))


class EmailNotification(Notification):
    def __init__(self, config, server):
        super().__init__(config, server)
        self.use_tls = config.getboolean('use_tls', False)
        self.use_starttls = config.getboolean('use_starttls', False)
        self.port = config.getint('port', 25)
        self.smtp_server = config.get('server', 'localhost')
        self.username = config.get('username')
        self.password = config.get('password')
        self.subject_template = config.get(
            'subject_template', 'Event notification for {thread_id}')
        self.from_address = config.get('from')
        self.to_address = config.get('to')

    async def _notify(self, source, thread_id, when, message_id, message):
        await super()._notify(source, thread_id, when, message_id, message)

        async with aiosmtplib.SMTP(hostname=self.smtp_server, port=self.port, use_tls=self.use_tls) as smtp:
            if self.use_starttls:
                await smtp.starttls()

            if self.username and self.password:
                await smtp.login(self.username, self.password)

            msg = email.message.EmailMessage()
            msg['Subject'] = self.subject_template.format(
                thread_id=thread_id, message_id=message_id,
                when=time_utils.time_to_human(when), source=source)
            msg['From'] = self.from_address
            msg['To'] = self.to_address
            msg['Message-ID'] = '<{}.{}>'.format(
                message_id, self.from_address)
            if message_id != thread_id:
                msg['References'] = msg['In-Reply-To'] = '<{}.{}>'.format(
                    thread_id, self.from_address)

            msg.set_content(message)

            await smtp.send_message(msg)

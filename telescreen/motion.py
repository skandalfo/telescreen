import aiofiles
from configparser import _UNSET
import os
import tempfile

from telescreen import receiver, child_process
from telescreen.serverobject import kill_server_on_exception, line_processor_loop


class MotionReceiver(receiver.Receiver):
    def __init__(self, config, server):
        super().__init__(config, server)
        self.motion_binary = config.get('motion_binary', 'motion')
        self.signal_name = config.get('signal_name', _UNSET)
        self.signal = self.server.add_signal(self.signal_name)

        self._motion_conf = {}
        for method, keys in (
            (config.get, (
                'netcam_url',
                'netcam_userpass',
                'despeckle_filter',
            )),
            (config.getboolean, (
                'netcam_keepalive',
                'netcam_tolerant_check',
                'rtsp_uses_tcp',
                'threshold_tune',
                'noise_tune',
            )),
            (config.getint, (
                'log_level',
                'width',
                'height',
                'framerate',
                'threshold',
                'noise_level',
                'smart_mask_speed',
                'lightswitch',
                'minimum_motion_frames',
            )),
        ):
            for key in keys:
                self._motion_conf[key] = method(key, None)
        self._motion_conf.update({
            'daemon': False,
            'pre_capture': 0,
            'post_capture': 0,
            'event_gap': 0,
            'output_pictures': False,
            'output_debug_pictures': False,
            'ffmpeg_output_movies': False,
            'ffmpeg_output_debug_movies': False,
            'quiet': True,
            'on_motion_detected': 'echo MOTION_DETECTED',
        })
        self._subprocess = child_process.ChildProcessLoop(
            config, server, 'motion', stdout=line_processor_loop(
                self._process_motion_output))

    async def _create_motion_conf(self):
        fd, self._motion_conf_path = tempfile.mkstemp(text=True)
        async with aiofiles.open(fd, mode='wt') as f:
            for key, value in self._motion_conf.items():
                if isinstance(value, bool):
                    await f.write('{} {}\n'.format(key, 'on' if value else 'off'))
                elif isinstance(value, str) or isinstance(value, int):
                    await f.write('{} {}\n'.format(key, value))

    async def _delete_motion_conf(self):
        os.remove(self._motion_conf_path)

    async def _start(self):
        await super()._start()
        await self._create_motion_conf()
        self._subprocess.command_line = self.command_line
        await self._subprocess.start()

    async def _stop(self):
        await self._subprocess.stop()
        await self._delete_motion_conf()
        await super()._stop()

    @kill_server_on_exception
    async def _process_motion_output(self, text):
        if text == 'MOTION_DETECTED':
            await self.signal.trigger(self.signal_duration)

    @property
    def command_line(self):
        return [self.motion_binary, '-c', self._motion_conf_path]

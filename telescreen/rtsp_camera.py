from builtins import property
from configparser import _UNSET

from telescreen import camera


class RTSPCamera(camera.Camera):
    def __init__(self, config, server):
        super().__init__(config, server)
        self.url = config.get('url', _UNSET)
        self.rtsp_transport = config.get('rtsp_transport', 'tcp')

    @property
    def input_args(self):
        return (
            [
                '-rtsp_transport', self.rtsp_transport,
                '-i', self.url
            ]
        )

import os.path
from pkg_resources import resource_string
import signal
import sys

from argparse import ArgumentParser
import asyncio
import functools
import logging
import subprocess
from telescreen import log_utils
from telescreen.config_utils import ConfigParser
from telescreen.log_utils import F
from telescreen.server import TeleScreenServer
import uvloop


__all__ = [
    'auth_manager',
    'camera',
    'child_process',
    'config_utils',
    'event',
    'log_utils',
    'm3u8',
    'motion',
    'notification',
    'periodic_task',
    'receiver',
    'resource_utils',
    'rtsp_camera',
    'schema',
    'server',
    'serverobject',
    'signals',
    'time_utils',
    'web_utils',
]


def install_systemd_service(systemd_user, config_paths, http_port, https_port):
    values = dict(
        executable_path=os.path.abspath(sys.argv[0]),
        configuration_paths=' '.join(
            [repr(os.path.abspath(path)) for path in config_paths]),
        daemon_user=systemd_user,
        http_port=http_port,
        https_port=https_port
    )

    unit_template = resource_string(
        __name__, 'telescreen.service.template').decode('utf-8')
    socket_template = resource_string(
        __name__, 'telescreen.socket.template').decode('utf-8')

    unit_file_contents = unit_template.format(**values)
    socket_file_contents = socket_template.format(**values)

    systemd_system_unit_dir = subprocess.check_output(
        ['pkg-config', '--variable=systemdsystemunitdir', 'systemd']).decode('utf-8').strip()
    systemd_service_file_path = os.path.join(
        systemd_system_unit_dir, 'telescreen.service')
    systemd_socket_file_path = os.path.join(
        systemd_system_unit_dir, 'telescreen.socket')

    logging.info(
        F('Installing systemd service: {0!r}', systemd_service_file_path))
    with open(systemd_service_file_path, 'wt') as f:
        f.write(unit_file_contents)

    logging.info(
        F('Installing systemd socket: {0!r}', systemd_socket_file_path))
    with open(systemd_socket_file_path, 'wt') as f:
        f.write(socket_file_contents)


def main():
    try:
        # Ensure we import aiocontextvars first.
        import aiocontextvars
        # Use uvloop for the event loop.
        asyncio.set_event_loop_policy(uvloop.EventLoopPolicy())
        loop = asyncio.get_event_loop()

        # Setup argument parser
        args_parser = ArgumentParser(
            prog='telescreen',
            description='IP camera management daemon')
        args_parser.add_argument(
            '-c',
            '--config',
            dest='config_path',
            nargs='+',
            default=[
                '/etc/telescreen/telescreen.ini',
                os.path.expanduser('~/.telescreen/telescreen.ini')],
            help='path to the configuration file [default: %(default)s]')
        args_parser.add_argument(
            '-l',
            '--loglevel',
            dest='log_level',
            type=int,
            default=logging.INFO,
            help='logging level [default: %(default)s]')
        args_parser.add_argument(
            '--install_systemd_service',
            dest='install_systemd_service',
            action='store_true',
            help='install a systemd service unit for Telescreen')
        args_parser.add_argument(
            '--systemd_user',
            dest='systemd_user',
            default='telescreen',
            help='user to run the systemd service as [default: %(default)s]')
        args_parser.add_argument(
            '--systemd_http_port',
            dest='systemd_http_port',
            type=int,
            default=8080,
            help='TCP port for HTTP in the systemd socket [default: %(default)s]')
        args_parser.add_argument(
            '--systemd_https_port',
            dest='systemd_https_port',
            type=int,
            default=8443,
            help='TCP port for HTTPS in the systemd socket [default: %(default)s]')

        # Process arguments
        args = args_parser.parse_args()

        # Set up logging
        log_utils.ConfigLogging(args.log_level)

        config_paths = args.config_path

        if args.install_systemd_service:
            systemd_user = args.systemd_user
            http_port = args.systemd_http_port
            https_port = args.systemd_https_port
            install_systemd_service(
                systemd_user, config_paths, http_port, https_port)
            return

        logging.info(F('Python path: {#W:{0!r}#}', sys.path))
        logging.info(F('Reading configs: {#W:{0!r}#}', config_paths))
        config_parser = ConfigParser()
        if not config_parser.read(config_paths):
            raise OSError(
                'Cannot read any configuration file from {#W:{0!r}#}.'.format(
                    config_paths))

        servers = config_parser.construct('telescreen.server.TeleScreenServer')

        async def _stop_by_signal(signum):
            logging.info(F('Stopping by signal {#e:{0}#}...', signum))
            await asyncio.gather(*[server.stop() for server in servers.values()])

        for signum in (signal.SIGTERM, signal.SIGINT):
            loop.add_signal_handler(
                signum,
                functools.partial(
                    asyncio.ensure_future,
                    _stop_by_signal(signum)))

        try:
            loop.run_until_complete(asyncio.gather(
                *[server.run() for server in servers.values()]))

            # Finish pending tasks.
            pending = asyncio.all_tasks()
            loop.run_until_complete(asyncio.gather(*pending))
        except asyncio.CancelledError:
            pass

        return
    except Exception:
        logging.exception('Exiting due to exception')
        return 1

from collections import OrderedDict
import configparser
import logging
import pkg_resources

from telescreen import time_utils
from telescreen.log_utils import F


def resolve_set(entries, resolver=lambda x: [x]):
    result = set()
    for entry in entries:
        result.update(set(resolver(entry)))
    return result


def resolve_mapping(
        entries,
        key_resolver=lambda x: [x],
        value_resolver=lambda x: [x]):
    result = {}
    for keys, values in entries:
        resolved_values = resolve_set(values, resolver=value_resolver)
        resolved_keys = resolve_set(keys, resolver=key_resolver)
        for key in resolved_keys:
            result_values = result.setdefault(key, set())
            result_values.update(resolved_values)
    return result


def parse_list(text):
    return text.split()


def parse_mapping(text):
    entries = [
        entry
        for entry in [maybe_entry.strip() for maybe_entry in text.split(',')]
        if entry]

    result = []
    for entry in entries:
        left, right = entry.split('->')
        result.append((parse_list(left), parse_list(right)))

    return result


class ConfigParser(configparser.ConfigParser):
    def __init__(self):
        super().__init__(
            converters={
                'duration': time_utils.duration,
                'list': parse_list,
                'mapping': parse_mapping,
            },
            interpolation=configparser.ExtendedInterpolation())
        self.logger = logging.getLogger(self.__class__.__name__)

    def copy(self, defaults_from_section=configparser.DEFAULTSECT):
        result = ConfigParser()

        data = OrderedDict()
        for section_name in self.sections():
            section = self[section_name]
            section_data = OrderedDict()
            for option in section:
                original_value = section.get(option, raw=True, fallback=None)
                if original_value is not None:
                    section_data[option] = original_value
            data[section_name] = section_data

        if self.has_section(defaults_from_section):
            default_data = OrderedDict()
            for option, raw_value in self.items(
                    defaults_from_section, raw=True):
                default_data[option] = raw_value
            data[configparser.DEFAULTSECT] = default_data

        result.read_dict(data)
        return result

    def construct(self, group_name, *args, filters={}, **kwargs):
        objects = {}
        for entry_point in pkg_resources.iter_entry_points(group_name):
            class_name = entry_point.name
            self.logger.info(
                F('Searching for configured instances of class {#t:{}#}...', class_name))
            cls = entry_point.load()
            prefix = '{}.'.format(class_name)
            defaults_section_name = class_name

            for section_name in self:
                if section_name.startswith(
                        prefix) and section_name != defaults_section_name:
                    object_name = section_name[len(prefix):]
                    object_config = self.copy(defaults_section_name)
                    section = object_config[section_name]
                    if {k: section.get(k, None) for k in filters} != filters:
                        # Skip this section.
                        continue
                    self.logger.info(
                        F('Parsing instance from section named: {#T:{}#}', section_name))
                    section['name'] = object_name
                    objects[object_name] = cls(section, *args, **kwargs)

        return objects

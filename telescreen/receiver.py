import aiosmtpd.smtp
from configparser import _UNSET
import fnmatch
import functools

from telescreen import config_utils
from telescreen.log_utils import F
from telescreen.serverobject import ServerObject, kill_server_on_exception


class Receiver(ServerObject):
    def __init__(self, config, server):
        super().__init__(config['name'], server)
        self.signal_duration = config.getduration('signal_duration', _UNSET)


class EmailReceiver(Receiver):
    def __init__(self, config, server):
        super().__init__(config, server)
        self.port = config.getint('port', 2525)
        self.address = config.get('address', None)
        self.sender_signals = config_utils.resolve_mapping(
            config.getmapping('sender_signals', []),
            value_resolver=lambda name: [self.server.add_signal(name)])
        self.recipient_signals = config_utils.resolve_mapping(
            config.getmapping('recipient_signals', []),
            value_resolver=lambda name: [self.server.add_signal(name)])

    @kill_server_on_exception
    async def handle_DATA(self, unused_server, unused_session, envelope):
        signals = set()
        for pattern, signal_set in self.sender_signals.items():
            if fnmatch.fnmatchcase(envelope.mail_from, pattern):
                signals.update(signal_set)
        for recipient in envelope.rcpt_tos:
            for pattern, signal_set in self.recipient_signals.items():
                if fnmatch.fnmatchcase(recipient, pattern):
                    signals.update(signal_set)

        if signals:
            await self.gather(
                *[signal.trigger(self.signal_duration)
                  for signal in signals])
        else:
            self.logger.warning(
                F(
                    'Message from {#R:{0!r}#} to {#R:{1!r}#} matched no signals.',
                    envelope.mail_from,
                    envelope.rcpt_tos))

        return '250 Message accepted for delivery'

    async def _start(self):
        await super()._start()
        self._smpt_server = await self.server.loop.create_server(
            functools.partial(aiosmtpd.smtp.SMTP, handler=self), host=self.address, port=self.port)

    async def _stop(self):
        self._smpt_server.close()
        await self._smpt_server.wait_closed()
        await super()._stop()

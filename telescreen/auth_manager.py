import aiohttp_jinja2
import aiohttp_security.abc
import aiohttp_session.cookie_storage
import base64
from cryptography import fernet

from aiohttp import hdrs
from aiohttp.web import middleware, HTTPUnauthorized, HTTPFound, HTTPMovedPermanently

from telescreen import config_utils, time_utils, web_utils
from telescreen.log_utils import F
from telescreen.serverobject import ServerObject


class AuthManager(
        ServerObject,
        aiohttp_security.abc.AbstractAuthorizationPolicy):
    resources = web_utils.WebResources()

    def __init__(self, config, server):
        super().__init__(config['name'], server)
        self.auth_pairs = config_utils.resolve_mapping(
            config.getmapping('auth_pairs', {}))
        self.permissions = config_utils.resolve_mapping(
            config.getmapping('permissions', {}))
        self.transport_security_max_age = config.getduration(
            'transport_security_max_age', time_utils.duration('1y'))
        # cookie_fernet_key must be 32 byte url-safe base64-encoded string
        self.cookie_fernet_key = config.get(
            'cookie_fernet_key', fernet.Fernet.generate_key())
        self.public_resources = set()
        self.unredirected_resources = set()

    def setup(self, app, force_https=False):
        storage = aiohttp_session.cookie_storage.EncryptedCookieStorage(
            self.cookie_fernet_key, cookie_name='TELESCREEN_SESSION')
        aiohttp_session.setup(app, storage)

        identity_policy = aiohttp_security.SessionIdentityPolicy()
        aiohttp_security.setup(app, identity_policy, self)

        # Set up all web handlers.
        self.resources.setup(self, app, self)

        # Redirect http->https if both ports are set up.
        if force_https:
            app.middlewares.append(self._ensure_https)
        app.middlewares.append(self._intercept)

    @middleware
    async def _intercept(self, request, handler):
        resource = request.match_info.route.resource

        try:
            # Enforce "login required" everywhere except public routes.
            if resource not in self.public_resources:
                await aiohttp_security.check_authorized(request)

            return await handler(request)
        except HTTPUnauthorized:
            # If marked as no-redirect, or not a GET request, just re-raise.
            if request.method != hdrs.METH_GET or resource in self.unredirected_resources:
                raise

            # Render the login page instead.
            request['from_url'] = request.url
            return await self._display_login_form(request)

    @middleware
    async def _ensure_https(self, request, handler):
        if request.scheme == 'http':
            new_request = request.clone(scheme='https')
            raise HTTPMovedPermanently(new_request.url)

        response = await handler(request)
        response.headers['Strict-Transport-Security'] = 'max-age={0:d}'.format(
            int(self.transport_security_max_age.total_seconds()))
        return response

    @aiohttp_jinja2.template('login.jinja2')
    async def _display_login_form(self, request):
        from_url = request.get('from_url',
                               request.app.router['root'].url_for())
        error_message = request.get('error_message')
        username = request.get('username', '')

        return dict(
            error_message=error_message,
            from_url=from_url,
            username=username,
            post_url=request.app.router['login'].url_for())

    @resources.register(
        '/login',
        name='login',
        public=True,
        methods=[hdrs.METH_POST])
    async def _handle_login(self, request):
        form = await request.post()
        username = form.get('username')
        password = form.get('password')
        from_url = form.get('from_url', request.app.router['root'].url_for())

        if username in self.auth_pairs and password in self.auth_pairs[username]:
            # Successful authentication.
            response = HTTPFound(from_url)
            await aiohttp_security.remember(request, response, username)
            self.logger.info(F('Login succeeded for {#*T:{}#}.', username))
            return response
        else:
            # Authentication failed; display the login form again.
            request['username'] = username
            request['from_url'] = from_url
            request['error_message'] = 'Login failed!'
            self.logger.warning(F('Login failed for {#*T:{}#}.', username))
            return await self._display_login_form(request)

    @resources.register(
        '/logout',
        name='logout',
        navbar_name='Logout',
        navbar_class='logout')
    async def _handle_logout(self, request):
        identity = await aiohttp_security.authorized_userid(request)
        response = HTTPFound(request.app.router['root'].url_for())
        await aiohttp_security.forget(request, response)
        if identity:
            self.logger.info(F('Logout succeeded for {#*T:{}#}.', identity))

        raise response

    async def authorized_userid(self, identity):
        return identity if identity in self.auth_pairs else None

    async def permits(self, identity, permission, unused_context=None):
        return permission in self.permissions.getdefault(identity, {})

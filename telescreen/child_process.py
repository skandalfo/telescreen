import asyncio
from configparser import _UNSET

from telescreen.log_utils import F
from telescreen.serverobject import ServerObject, kill_server_on_exception, DEVNULL, STDOUT, LOG_INFO


async def do_nothing():
    pass


class ChildProcessLoop(ServerObject):
    def __init__(self, config, server, binary_name, *args, pre_loop=do_nothing, post_loop=do_nothing, stdin=DEVNULL, stdout=DEVNULL, stderr=LOG_INFO, terminate_signals = None):
        super().__init__('{}_{}'.format(config['name'], binary_name), server)
        failure_signal_name = config.get(
            '{}_failure_signal_name'.format(binary_name))
        if failure_signal_name is not None:
            self.logger.info(
                F('Failure signal name: {#e:{}#}', failure_signal_name))
            self.failure_signal = server.add_signal(
                failure_signal_name)
        else:
            self.failure_signal = None

        self.restart_delay = config.getduration(
            '{}_restart_delay'.format(binary_name), _UNSET)
        self.kill_delay = config.getduration(
            '{}_kill_delay'.format(binary_name), _UNSET)

        self.command_line = args
        self.terminate_signals = terminate_signals
        self.pre_loop = pre_loop
        self.post_loop = post_loop
        self.stdin_handler = stdin
        self.stdout_handler = stdout
        self.stderr_handler = stderr
        
        self._subprocess = None
        self.subprocess_running = asyncio.Event()

    async def _start(self):
        await super()._start()
        self._loop_future = self.spawn(self._loop())

    async def _stop(self):
        await self.cancel(self._loop_future)
        await self.terminate_subprocess()
        await super()._stop()

    async def terminate_subprocess(self):
        tmp = self._subprocess
        if tmp:
            await tmp.terminate()
            await tmp.exit_code()

    @kill_server_on_exception
    async def _loop(self):
        while True:
            await self.pre_loop()

            subprocess = await self.spawn_subprocess(*self.command_line,
                                                     stdin=self.stdin_handler,
                                                     stdout=self.stdout_handler,
                                                     stderr=self.stderr_handler,
                                                     kill_delay=self.kill_delay,
                                                     terminate_signals = self.terminate_signals)
            self._subprocess = subprocess

            try:
                exit_code = '<unset>'
                try:
                    self.subprocess_running.set()
                    exit_code = await subprocess.exit_code()
                finally:
                    self.subprocess_running.clear()

                # For some reason we can get None as the exit_code here when the
                # server is shutting down. Just finish the loop.
                if exit_code is None:
                    return

                self.logger.error(
                    F('Restarting after child process exited with error code {#e:{}#}',
                      exit_code))
                if self.failure_signal:
                    await self.failure_signal.trigger(self.restart_delay * 2)

                await self.post_loop()

            except Exception:
                await subprocess.terminate()
                await subprocess.exit_code()
                raise

            await self.sleep(self.restart_delay)

import datetime

import asyncio
from configparser import _UNSET
from telescreen import time_utils, config_utils, schema, periodic_task
from telescreen.db_utils import serializable_transaction, TransactionContext
from telescreen.log_utils import F
from telescreen.serverobject import ServerObject


_VALID_TRANSITION_NAMES = {
    'open', 'change', 'new', 'resolve', 'reopen', 'close', 'remind'
}

_TRANSITIONS_THAT_IMPLY_CHANGE = {'open', 'new', 'change', 'resolve', 'reopen'}

_TRANSITION_NAME_SET_ALIASES = {
    'all': _VALID_TRANSITION_NAMES,
    'event': {'open', 'close'},
    'signals': _TRANSITIONS_THAT_IMPLY_CHANGE,
}

_TRANSITION_MESSAGES = {
    'open': 'A new event has been triggered.',
    'new': 'New signals have become active for this event.',
    'change': 'The signal set has changed for this event.',
    'resolve': 'All the signals have cleared for this event.',
    'reopen': 'This event has been reopened by a new signal.',
    'close': 'This event has been closed.',
    'remind': 'This is a reminder for an event that is still open.',
}


class Event(ServerObject):
    def __init__(self, config, server):
        super().__init__(config['name'], server)
        self.close_time = config.getduration(
            'close_time', time_utils.duration('0s'))
        self.remind_time = config.getduration('remind_time', None)
        self.retention_time = config.getduration('retention_time', None)

        # Parse signals.
        self.signals = config_utils.resolve_set(
            config.getlist('signals', _UNSET),
            lambda pattern: self.server.get_signals(pattern))

        # Parse the cameras the event should capture.
        self.cameras = config_utils.resolve_set(
            config.getlist('cameras', ''),
            lambda pattern: self.server.get_cameras(pattern))

        # Parse notification rules.
        def resolve_alias(alias):
            if alias in _TRANSITION_NAME_SET_ALIASES:
                return _TRANSITION_NAME_SET_ALIASES[alias]
            elif alias in _VALID_TRANSITION_NAMES:
                return [alias]
            else:
                raise KeyError(
                    'Unknown transition for notification: {0}.'.format(alias))

        self.notifications = config_utils.resolve_mapping(
            config.getmapping('notifications', _UNSET),
            key_resolver=resolve_alias,
            value_resolver=lambda pattern: self.server.get_notifications(
                pattern))

        self.garbage_collect_period = config.getduration(
            'garbage_collect_period', time_utils.duration('5m'))

        # Periodically try to close open events.
        self._expiration_processor = periodic_task.PeriodicTask(
            self.server,
            self.name + '_expiry',
            self.server.signal_scan_period,
            self._check_expiration)

        # GC periodic task.
        self._gc_task = periodic_task.PeriodicTask(
            self.server,
            self.name + '_gc',
            self.garbage_collect_period,
            self._garbage_collect_old_events)

    async def _start(self):
        await super()._start()

        for signal in self.signals:
            signal.subscribe(self)

        await self._expiration_processor.start()
        await self._gc_task.start()

    async def _stop(self):
        await self._gc_task.stop()
        await self._expiration_processor.stop()

        for signal in self.signals:
            signal.unsubscribe(self)

        await super()._stop()

    async def _get_db_event(self):
        return await self.server.db.first(
            schema.Event.query
            .where((schema.Event.name == self.name) & (schema.Event.expired == None))  # noqa: E711
            .order_by(schema.Event.start.desc())
            .limit(1)
        )

    async def _get_db_signal(self, event, signal_name):
        return await self.server.db.first(
            schema.EventSignal.query
            .where(
                (schema.EventSignal.end == None) &  # noqa: E711
                (schema.EventSignal.event_id == event.id) &
                (schema.EventSignal.signal_name == signal_name)
            )
            .limit(1)
        )

    async def _get_db_active_signals(self, event):
        return await self.server.db.all(
            schema.EventSignal.query
            .where(
                (schema.EventSignal.end == None) &  # noqa: E711
                (schema.EventSignal.event_id == event.id)
            )
            .order_by(schema.EventSignal.signal_name)
        )

    @serializable_transaction()
    async def _garbage_collect_old_events(self):
        if not self.retention_time:
            return

        now = datetime.datetime.now(tz=datetime.timezone.utc)
        retain_since = now - self.retention_time

        # Remove all the entries from the database.
        delete_query = schema.Event.delete.where(
            schema.Event.name == self.name).where(
            schema.Event.end < retain_since)

        # This via cascading will also erase dependent EventSegmentLock entries
        # and allow garbage-collecting camera segments too.
        await self.server.db.status(delete_query)

    @serializable_transaction()
    async def _check_expiration(self):
        when = datetime.datetime.now(tz=datetime.timezone.utc)
        event = await self._get_db_event()
        if not event:
            return

        deadlines = []

        if event.expiry is not None and event.expiry <= when:
            self.logger.info(
                F('Event {#T:{}#} has expired on {#E:{}#}.', self.name, event.expiry))

            # Mark the event as closed in the database.
            await event.update(expired=event.expiry, expiry=None).apply(bind=self.server.db)

            # Send notifications for the closure of our event.
            await self._send_notifications(event, when, 'close')

            # Restart in a different transaction.
            return datetime.timedelta()
        else:
            if (event.last_notification is not None and
                self.remind_time is not None and
                    when >= event.last_notification + self.remind_time):
                await self._send_notifications(event, when, 'remind')

                # Restart in a different transaction.
                return datetime.timedelta()

        # Add any deadlines for this event.
        if event.expiry is not None:
            deadlines.append(event.expiry)
        if event.last_notification is not None and self.remind_time is not None:
            deadlines.append(event.last_notification + self.remind_time)

        # Wait for the next one.
        if deadlines:
            return min(deadlines) - when

    async def _send_notifications(self, event, when, transition):
        thread_id = '{}/{}'.format(self.name, event.start.timestamp())
        event_id = '{}/{}'.format(self.name, when.timestamp())

        current_signals = await self._get_db_active_signals(event)

        if not current_signals and event.expiry is None and event.expired is None:
            # Mark the event's end and ensure we expire the event later.
            await event.update(expiry=when + self.close_time, end=when).apply(bind=self.server.db)
        elif current_signals and event.expiry is not None:
            # Don't leave the event expire as long as there are active signals.
            await event.update(expiry=None, end=None).apply(bind=self.server.db)

        # Adjust the kind of transition if needed.
        if transition == 'new' and len(current_signals) == 1:
            if event.start == when:
                transition = 'open'
            else:
                transition = 'reopen'
        elif transition == 'resolve' and current_signals:
            transition = 'change'

        # Calculate the set of notifications to send.
        notifications = set()
        notifications.update(self.notifications.get(transition, []))

        if transition == 'new' and len(current_signals) == 1:
            open_transition = 'open' if event.start == when else 'reopen'
            notifications.update(self.notifications.get(open_transition, []))

        if transition == 'change' and len(current_signals) == 0:
            notifications.update(self.notifications.get('close', []))

        if transition in _TRANSITIONS_THAT_IMPLY_CHANGE:
            notifications.update(self.notifications.get('change', []))

        # Return if nothing to notify.
        if not notifications:
            return

        # Prepare a notification message.
        lines = [
            'Event {}: {}'.format(
                thread_id,
                _TRANSITION_MESSAGES[transition]),
            '  Notification sent on: {}'.format(
                time_utils.time_to_human(when)),
        ]

        lines.append(
            '  Currently active signals: {}'.format(
                ', '.join(signal.signal_name for signal in current_signals)))

        message = '\n'.join(lines)

        # Send notifications out. Do it iteratively as we are likely running
        # within a transaction and we can't share a single DB connection among
        # many separate coroutines.
        for notification in notifications:
            await notification.notify(self.name, thread_id, when, event_id, message)

        # Time-keeping for reminders.
        await event.update(last_notification=when).apply(bind=self.server.db)

    @serializable_transaction()
    async def signal_cleared(self, signal, when):
        # Find the existing event.
        event = await self._get_db_event()
        if event:
            # Find the signal state therein.
            db_signal = await self._get_db_signal(event, signal.name)
            if db_signal:
                # Mark the signal in the database as ended.
                await db_signal.update(end=when).apply(self.server.db)

                # Trigger any notification.
                await self._send_notifications(event, when, 'resolve')

        # Awaken the queue processor on commit.
        TransactionContext.get_active().run_after_commit(
            self._expiration_processor.awaken())

    @serializable_transaction()
    async def signal_triggered(self, signal, when):
        # Find an existing event or create a new one.
        event = await self._get_db_event()
        if not event:
            # We need to create a new one.
            event = await schema.Event(name=self.name, start=when).create(bind=self.server.db)

            # And lock camera segments as needed.
            for camera in self.cameras:
                await schema.EventSegmentLock(
                    source=camera.name, event_id=event.id).create(
                    bind=self.server.db)

        # Create a new signal segment therein.
        await schema.EventSignal(event_id=event.id, signal_name=signal.name, start=when).create(bind=self.server.db)

        # Trigger any notification.
        await self._send_notifications(event, when, 'new')

    @serializable_transaction()
    async def get_event_history(self, start, end):
        start = start or datetime.datetime.min
        end = end or datetime.datetime.max

        query = schema.Event.outerjoin(
            schema.EventSignal).select().where(
            ((schema.Event.start < end) & (
                (schema.Event.expired > start) | (schema.Event.expired == None))) | (  # noqa: E711
                (schema.EventSignal.start < end) & (
                    (schema.EventSignal.end > start) | (schema.EventSignal.end == None)))).where(  # noqa: E711
                        schema.Event.name == self.name)

        query = query.execution_options(
            loader=schema.Event.distinct(
                schema.Event.id).load(
                add_signal=schema.EventSignal))

        events = await self.server.db.all(query)

        return {
            event.id: {
                'start': time_utils.time_to_json(event.start),
                'end': time_utils.time_to_json(event.end),
                'signals': {
                    signal.id: {
                        'name': signal.signal_name,
                        'start': time_utils.time_to_json(signal.start),
                        'end': time_utils.time_to_json(signal.end)
                    }
                    for signal in event.signals
                }
            }
            for event in events
        }

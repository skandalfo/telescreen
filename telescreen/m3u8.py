import datetime
import math

import collections
import functools
import inspect
import logging
import re
from telescreen import time_utils
from telescreen.log_utils import F


class Entry(object):
    def __init__(self, name, is_new_segment, duration, time, title):
        self.name = name
        self.is_new_segment = is_new_segment
        self.duration = duration
        self.time = time
        self.title = title


class PlayList(object):
    _handlers = {}

    def __init__(self):
        self.entries = []
        self.version = 3
        self.targetduration = datetime.timedelta(seconds=0)
        self.media_sequence = 0
        self.finished = False

    def append(self, other):
        self.targetduration = max(self.targetduration, other.targetduration)

        new_entries = other.entries

        if self.entries:
            last_segment_name = self.entries[-1].name
            for i in reversed(range(len(new_entries))):
                if new_entries[i].name == last_segment_name:
                    new_entries = new_entries[i + 1:]
                    break

        self.entries.extend(new_entries)

    def lines(self):
        yield '#EXTM3U'
        yield '#EXT-X-VERSION:{}'.format(self.version)
        yield '#EXT-X-TARGETDURATION:{}'.format(self.targetduration.total_seconds())
        yield '#EXT-X-MEDIA-SEQUENCE:{}'.format(self.media_sequence)

        for entry in self.entries:
            if entry.is_new_segment:
                yield '#EXT-X-DISCONTINUITY'
            yield '#EXT-X-PROGRAM-DATE-TIME:{}'.format(time_utils.time_to_iso(entry.time))
            yield '#EXTINF:{},{}'.format(entry.duration.total_seconds(), entry.title)
            yield entry.name

        if self.finished:
            yield '#EXT-X-ENDLIST'

    def text(self):
        return ''.join(line + '\n' for line in self.lines())

    async def write(self, f):
        await f.write(self.text())

    def from_segments(self, segments):
        self.entries = []
        self.media_sequence = segments[0].sequence
        self.targetduration = datetime.timedelta(seconds=math.ceil(
            max((segment.end - segment.start).total_seconds() for segment in segments)))
        for segment in segments:
            self.entries.append(
                Entry(
                    segment.name,
                    segment.new_segment,
                    segment.end - segment.start,
                    segment.start,
                    segment.title))
        if self.entries:
            self.entries[0].is_new_segment = True

    async def read(self, f):
        # Initialize our state.
        self.entries = []
        self._header = False
        self.version = None
        self.targetduration = None
        self.media_sequence = None
        self.finished = False
        self._state = Entry(None, True, None, 0.0, '')
        self._have_date_time = False

        text = await f.read()

        def to_arg(name):
            return name.lower().replace('-', '_')

        def convert(parameter, value):
            annotation = parameter.annotation
            if annotation == inspect.Parameter.empty:
                logging.warning(
                    F('Parsing argument {} with value {} as str by lack of annotations.', parameter.name, value))
                return value
            try:
                return annotation(value)
            except Exception:
                logging.exception(
                    F('Cannot parse {} of type {} from value "{}".', parameter.name, annotation, value))
                raise

        cls = self.__class__

        for match in re.finditer('[^\n]*\n', text):
            l = match.group(0).strip()
            if not l:
                continue

            if l.startswith('#'):
                l = l[1:]
                i = l.find(':')
                if i < 0:
                    key, arg_line = l, None
                else:
                    key, arg_line = l[:i], l[i + 1:]

                if key in cls._handlers:
                    handler, kw_params = cls._handlers[key]
                else:
                    handler_name = '_parse_{}'.format(
                        to_arg(key))
                    handler = getattr(
                        cls, handler_name, functools.partial(
                            cls._parse_unknown, key))
                    # Skip self, which would be the first arg.
                    skip = True
                    kw_params = collections.OrderedDict()
                    for k, v in inspect.signature(
                            handler).parameters.items():
                        if skip:
                            skip = False
                        else:
                            kw_params[k] = v
                    cls._handlers[key] = (handler, kw_params)

                args = []
                kwargs = {}
                pos_params = iter(kw_params.values())

                if arg_line:
                    try:
                        for item in arg_line.split(','):
                            if '=' in item:
                                name, value = item.split('=')
                                name = to_arg(name)
                                value = value.strip('"')
                                if name not in kw_params:
                                    logging.warning(
                                        F('Unknown argument {} of value {} for tag {}', name, value, key))
                                param = kw_params[name]
                                kwargs[name] = convert(param, value)
                            else:
                                value = item.strip('"')
                                try:
                                    param = next(pos_params)
                                    args.append(convert(param, value))
                                except StopIteration:
                                    logging.warning(
                                        F('Ignoring unexpected positional argument value {} for tag {}', value, key))
                    except Exception:
                        logging.exception(
                            F('Error while parsing {} arguments: {}', key, arg_line))
                        raise

                handler(self, *args, **kwargs)
            else:
                self._add_entry(l)

        self._check_main_headers()

    def _check_main_headers(self):
        if not self._header:
            raise ValueError('Missing #EXTM3U header.')

        if self.version is None:
            raise ValueError('Missing #EXT-X-VERSION header.')

        if self.targetduration is None:
            raise ValueError('Missing #EXT-X-TARGETDURATION header.')

        if self.media_sequence is None:
            raise ValueError('Missing #EXT-X-MEDIA-SEQUENCE header.')

    def _add_entry(self, filename):
        self._check_main_headers()

        current = self._state
        current.name = filename

        if self.finished:
            raise ValueError('Further segments found past #EXT-X-ENDLIST.')

        if current.duration is None:
            raise ValueError(
                'Could not find the duration of segment: {}; missing #EXTINF header?'.format(filename))

        self.entries.append(current)

        self._state = Entry(
            None, False, None, current.time + current.duration, '')
        self._have_date_time = False

    def _parse_extm3u(self):
        self._header = True

    def _parse_ext_x_version(self, version: int):
        self.version = version

    def _parse_ext_x_targetduration(self, targetduration: int):
        self.targetduration = datetime.timedelta(seconds=targetduration)

    def _parse_ext_x_media_sequence(self, media_sequence: int):
        self.media_sequence = media_sequence

    def _parse_ext_x_discontinuity(self):
        self._state.is_new_segment = True

    def _parse_extinf(self, duration: float, title: str):
        self._state.duration = datetime.timedelta(seconds=duration)
        self._state.title = title

    def _parse_ext_x_program_date_time(self, date_time: time_utils.iso_to_time):
        self._state.time = date_time
        self._have_date_time = True

    def _parse_ext_x_endlist(self):
        self.finished = True

    @staticmethod
    def _parse_unknown(key, self, *unused_args, **unused_kwargs):
        logging.warning(F('Unknown tag: {#r:{}#}', key))

    @property
    def files(self):
        for l in self.entries:
            yield l.name

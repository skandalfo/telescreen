import datetime

from telescreen import schema, periodic_task
from telescreen.db_utils import serializable_transaction
from telescreen.log_utils import F
from telescreen.serverobject import ServerObject


class Signal(ServerObject):
    def __init__(self, name, server):
        super().__init__(name, server)
        self._observers = set()

        # Periodically try to close pending signals.
        self._expiration_processor = periodic_task.PeriodicTask(
            self.server,
            self.name + '_expiry',
            self.server.signal_scan_period,
            self._check_expiration)

    def subscribe(self, observer):
        self._observers.add(observer)

    def unsubscribe(self, observer):
        self._observers.remove(observer)

    async def _start(self):
        await super()._start()
        await self._expiration_processor.start()

    async def _stop(self):
        await self._expiration_processor.stop()
        await super()._stop()

    async def _get_db_signal(self):
        return await self.server.db.first(
            schema.ActiveSignal.query
            .where(schema.ActiveSignal.name == self.name)
            .limit(1)
        )

    @serializable_transaction()
    async def _check_expiration(self):
        when = datetime.datetime.now(tz=datetime.timezone.utc)

        active_signal = await self._get_db_signal()
        if active_signal and active_signal.active_until:
            if active_signal.active_until <= when:
                self.logger.info(
                    F('Signal {#T:{}#} has expired on {#E:{}#}.', self.name, active_signal.active_until))

                # Notify observers.
                await self.gather(*[observer.signal_cleared(self, active_signal.active_until) for observer in self._observers])

                # Clear the signal active_until field.
                await active_signal.update(active_until=None).apply(bind=self.server.db)

                # Check again.
                return datetime.timedelta()
            else:
                # Check later.
                return active_signal.active_until - when

    @serializable_transaction()
    async def trigger(self, duration):
        when = datetime.datetime.now(tz=datetime.timezone.utc)
        new_expiry = when + duration
        self.logger.info(
            F('Signal {#T:{}#} notified for {#E:{}#} seconds on {#E:{}#}.', self.name, duration, when))

        active_signal = await self._get_db_signal()
        if not active_signal:
            active_signal = await schema.ActiveSignal(
                name=self.name).create(
                bind=self.server.db)

        is_new = (active_signal.active_until is None)

        # Update the expiry timestamp.
        await active_signal.update(active_until=new_expiry).apply(bind=self.server.db)

        # Notify the signal has been triggered. Iterate on observers as we need
        # to serialize the operations on the single database connection for the
        # transaction.
        if is_new:
            for observer in self._observers:
                await observer.signal_triggered(self, when)

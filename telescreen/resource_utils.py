import datetime
import hashlib
import mimetypes
import os

from aiohttp import web
import jinja2
import pkg_resources

from telescreen import time_utils, web_utils
from telescreen.log_utils import F
from telescreen.serverobject import ServerObject


class TemplateLoader(ServerObject, jinja2.ChoiceLoader):
    def __init__(self, config, server):
        ServerObject.__init__(self, config['name'], server)
        jinja2.ChoiceLoader.__init__(self, [
            jinja2.PackageLoader(package_instance.__name__, 'templates')
            for package_instance in self._list_packages('telescreen.Template')
        ])


class ResourceManager(ServerObject):
    resources = web_utils.WebResources()

    def __init__(self, config, server):
        super().__init__(config['name'], server)
        self.resource_timestamp = datetime.datetime.now(
            tz=datetime.timezone.utc)
        self.resource_cache_time = config.getduration(
            'resource_cache_time', time_utils.duration('1h'))
        self.resource_packages = []
        self._cache = {}

    def setup(self, app):
        self.resources.setup(self, app, self.server.auth_manager)

    async def _start(self):
        await super()._start()
        self.resource_packages = self._list_packages('telescreen.Resource')

    async def _stop(self):
        self.resource_packages = []
        self._cache = {}
        await super()._stop()

    async def _get_resource(self, path):
        async with self._lock:
            if path in self._cache:
                # We already had the resource cached.
                return self._cache[path]

            # We didn't look up these resources before.
            result = None
            file_path = os.path.join('resources', path)
            for p in self.resource_packages:
                package_name = p.__name__
                self.logger.info(
                    F('Trying to find resource for path {#W:{}#} in package {#t:{}#}...',
                      path, package_name))
                if pkg_resources.resource_exists(package_name, file_path):
                    result = pkg_resources.resource_string(
                        package_name, file_path)
                    self.logger.info(
                        F('Found resource for path {#W:{}#} in package {#t:{}#}.', path, package_name))
                    break

            if result is not None:
                mime_type, encoding = mimetypes.guess_type(
                    path)
                h = hashlib.sha256()
                h.update(result)
                etag = h.hexdigest()
            else:
                mime_type, encoding, etag = None, None, None

            all_results = (result, mime_type, encoding, etag)
            self._cache[path] = all_results

            return all_results

    @resources.register('/resources/{path:.*}', name='resources', public=True)
    async def _handle_resource(self, request):
        path = request.match_info['path']
        data, mime_type, encoding, etag = await self._get_resource(path)

        if data is None:
            raise web.HTTPNotFound()

        return await web_utils.serve_bytes(
            request, data, mtime=self.resource_timestamp, etag=etag,
            max_age=self.resource_cache_time,
            content_type=mime_type, encoding=encoding)

from gino import Gino
db = Gino()


class Segment(db.Model):
    __tablename__ = 'segments'

    source = db.Column(db.String(), primary_key=True)
    sequence = db.Column(db.BigInteger(), primary_key=True)
    new_segment = db.Column(db.Boolean(), nullable=False)
    name = db.Column(db.String(), nullable=False)
    start = db.Column(db.DateTime(timezone=True), nullable=False)
    end = db.Column(db.DateTime(timezone=True), nullable=False)
    title = db.Column(db.String())


_index_segments_by_start = db.Index(
    'index_segments_by_start',
    Segment.source,
    Segment.start
)

_index_segments_by_end = db.Index(
    'index_segments_by_end',
    Segment.source,
    Segment.end
)

_index_segments_new_segment = db.Index(
    'index_segments_new_segment',
    Segment.source,
    Segment.sequence,
    postgresql_where=(Segment.new_segment)
)


class ActiveSignal(db.Model):
    __tablename__ = 'active_signals'

    name = db.Column(db.String(), primary_key=True)
    active_until = db.Column(db.DateTime(timezone=True))


class Event(db.Model):
    __tablename__ = 'events'

    id = db.Column(db.Integer(), primary_key=True)
    name = db.Column(db.String(), nullable=False)
    start = db.Column(db.DateTime(timezone=True), nullable=False)
    end = db.Column(db.DateTime(timezone=True))
    expiry = db.Column(db.DateTime(timezone=True))
    expired = db.Column(db.DateTime(timezone=True))
    last_notification = db.Column(db.DateTime(timezone=True))

    def __init__(self, **kw):
        super().__init__(**kw)
        self._signals = set()

    @property
    def signals(self):
        return self._signals

    @signals.setter
    def add_signal(self, signal):
        self._signals.add(signal)


_index_events_by_start = db.Index(
    'index_events_by_start',
    Event.name,
    Event.start
)

_index_events_by_end = db.Index(
    'index_events_by_expired',
    Event.name,
    Event.expired
)

_index_events_only_one_active = db.Index(
    'index_events_only_one_active',
    Event.name,
    unique=True,
    postgresql_where=(Event.expired == None)  # noqa: E711
)


class EventSignal(db.Model):
    __tablename__ = 'event_signals'

    id = db.Column(db.Integer(), primary_key=True)
    event_id = db.Column(db.Integer(), nullable=False)
    signal_name = db.Column(db.String(), nullable=False)
    start = db.Column(db.DateTime(timezone=True), nullable=False)
    end = db.Column(db.DateTime(timezone=True))


_constraint_event_signals_to_event = db.ForeignKeyConstraint(
    [EventSignal.event_id],
    [Event.id],
    'constraint_event_signals_to_event',
    ondelete='CASCADE'
)

_index_event_signals_by_start = db.Index(
    'index_event_signals_by_start',
    EventSignal.event_id,
    EventSignal.signal_name,
    EventSignal.start
)

_index_event_signals_by_end = db.Index(
    'index_event_signals_by_end',
    EventSignal.event_id,
    EventSignal.signal_name,
    EventSignal.end
)

_index_event_signals_only_one_active = db.Index(
    'index_event_signals_only_one_active',
    EventSignal.event_id,
    EventSignal.signal_name,
    unique=True,
    postgresql_where=(EventSignal.end == None)  # noqa: E711
)


class EventSegmentLock(db.Model):
    __tablename__ = 'event_segment_locks'

    source = db.Column(db.String(), primary_key=True)
    event_id = db.Column(db.Integer(), primary_key=True)


_constraint_event_segment_locks_to_event = db.ForeignKeyConstraint(
    [EventSegmentLock.event_id],
    [Event.id],
    'constraint_event_segment_locks_to_event',
    ondelete='CASCADE'
)


class Notification(db.Model):
    __tablename__ = 'notifications'

    id = db.Column(db.Integer(), primary_key=True)
    name = db.Column(db.String(), nullable=False)
    pending = db.Column(db.Boolean(), nullable=False)
    failed = db.Column(db.Boolean(), nullable=False)
    next_retry = db.Column(db.DateTime(timezone=True), nullable=False)
    retry_count = db.Column(db.Integer())
    when = db.Column(db.DateTime(timezone=True), nullable=False)
    source = db.Column(db.String(), nullable=False)
    thread_id = db.Column(db.String(), nullable=False)
    message_id = db.Column(db.String(), nullable=False)
    message = db.Column(db.String(), nullable=False)


_index_notifications_by_next_retry = db.Index(
    'index_notifications_by_next_retry',
    Notification.name,
    Notification.pending,
    Notification.next_retry
)

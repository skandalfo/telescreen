import functools
import logging
import re

_ATTR_MAP = {
    '*': 1,  # Bold
    '.': 2,  # Faint
    '/': 3,  # Italic
    '_': 4,  # Underline
    '?': 5,  # Slow blink
    '!': 6,  # Fast blink
    '#': 7,  # Inverse video
    '-': 9,  # Crossed-out
}

_COLOR_MAP = {
    0: 0,
    1: 4,
    2: 1,
    3: 5,
    4: 2,
    5: 6,
    6: 3,
    7: 7,
}

for i, c in enumerate('qwertyui'):
    i = _COLOR_MAP[i]
    _ATTR_MAP[c] = 30 + i
    _ATTR_MAP[c.upper()] = 90 + i

for i, c in enumerate('asdfghjk'):
    i = _COLOR_MAP[i]
    _ATTR_MAP[c] = 40 + i
    _ATTR_MAP[c.upper()] = 100 + i

_MARK_RE = (
    r'\{\#([' +
    re.escape(''.join(_ATTR_MAP.keys())) +
    r']*):((?:(?!\#\}).)*)\#\}'
)
_TEXT_RE = (
    r'((?:(?!\{\#).)+)'
)
_COLORIZE_PARSE_RE = (
    _TEXT_RE + '|' + _MARK_RE
)


@functools.lru_cache(maxsize=3000)
def colorize(text):
    """Convert text coloring markers to ANSI sequences.

    '*': 1,  # Bold
    '.': 2,  # Faint
    '/': 3,  # Italic
    '_': 4,  # Underline
    '?': 5,  # Slow blink
    '!': 6,  # Fast blink
    '#': 7,  # Inverse video
    '-': 9,  # Crossed-out

             FG BFG  BG BBG
    Black     q   Q   a   A
    Blue      w   W   s   S
    Red       e   E   d   D
    Magenta   r   R   f   F
    Green     t   T   g   G
    Cyan      y   Y   h   H
    Yellow    u   U   j   J
    White     i   I   k   K
    """
    result = ''
    for match in re.finditer(_COLORIZE_PARSE_RE, text):
        lit, attrs, encl = match.group(1, 2, 3)
        if lit:
            result += lit
        if encl:
            result += '\033['
            result += ';'.join((str(_ATTR_MAP[v]) for v in attrs))
            result += 'm'
            result += encl
            result += '\033[0m'
    return result


class BraceMessage(object):
    def __init__(self, fmt, *args, **kwargs):
        self.fmt = colorize(fmt)
        self.args = args
        self.kwargs = kwargs

    def __str__(self):
        return self.fmt.format(*self.args, **self.kwargs)


F = BraceMessage


def ConfigLogging(log_level):
    logging.basicConfig(
        level=log_level,
        format=colorize(
            '{#*I:{asctime}#} {#*u:{levelname: <8}#} {#*/Y:{name}#}: {#i:{message}#}'),
        style='{')

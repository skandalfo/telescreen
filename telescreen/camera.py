import asyncio
import aiofiles
import aiohttp_jinja2
import aionotify
from builtins import property
import datetime
import os.path
import signal

from aiohttp import web

from configparser import _UNSET
import shutil
from telescreen import schema, m3u8, child_process, time_utils, web_utils, \
    periodic_task
from telescreen.db_utils import serializable_transaction
from telescreen.log_utils import F
from telescreen.schema import db
from telescreen.serverobject import ServerObject, kill_server_on_exception
import tempfile


PLAYLIST_FILENAME = 'hls.m3u8'
DOWNLOAD_FILENAME = 'download.mkv'
SEGMENT_FILENAME_PREFIX = 'segment'

SEGMENT_FILENAME_REGEXP = r'{}\.\d+\.ts'.format(SEGMENT_FILENAME_PREFIX)


class Camera(ServerObject):
    resources = web_utils.WebResources()

    def __init__(self, config, server):
        super().__init__(config['name'], server)

        self.camera_dir = config.get(
            'camera_dir', _UNSET)
        self.retention_time = config.getduration(
            'retention_time', _UNSET)
        self.segment_time = config.getduration('segment_time', _UNSET)
        self.stuckness_delay = config.getduration('stuckness_delay', _UNSET)

        self._watcher = aionotify.Watcher()
        self._watcher.watch(
            alias='capture', path=self.capture_dir,
            flags=aionotify.Flags.CLOSE_WRITE | aionotify.Flags.MOVED_TO)

        self.ffmpeg_binary = config.get('ffmpeg_binary', 'ffmpeg')
        self.ffmpeg_log_level = config.get('ffmpeg_log_level', 'info')
        self.url = config.get('url', _UNSET)
        self.rtsp_transport = config.get('rtsp_transport', 'tcp')
        self.hls_list_size = config.getint('hls_list_size', 5)
        self.playback_list_size = config.getint('playback_list_size', 5)
        self.garbage_collect_period = config.getduration(
            'garbage_collect_period', time_utils.duration('5m'))

        self._subprocess = child_process.ChildProcessLoop(
            config, server, 'ffmpeg', *self.command_line,
            pre_loop=self._pre_loop, post_loop=self._check_playlist,
            terminate_signals = [signal.SIGINT, signal.SIGTERM, signal.SIGKILL])

        self.subapp = self._make_app()

        # GC periodic task.
        self._gc_task = periodic_task.PeriodicTask(
            self.server,
            self.name + '_gc',
            self.garbage_collect_period,
            self._garbage_collect_old_segments)

    @property
    def streaming_delay(self):
        return self.segment_time

    @property
    def capture_dir(self):
        return os.path.join(self.camera_dir, 'capture')

    @property
    def storage_dir(self):
        return os.path.join(self.camera_dir, 'storage')

    @serializable_transaction()
    async def get_recorded_intervals(self, start, end):
        start = start or datetime.datetime.min
        end = end or datetime.datetime.max

        ranked_intervals = (
            db.select([
                schema.Segment.start.label('start'),
                schema.Segment.end.label('end'),
                db.func.count()
                .filter(schema.Segment.new_segment)
                .over(
                    partition_by=[schema.Segment.source],
                    order_by=[schema.Segment.sequence]
                )
                .label('run')
            ])
            .where((schema.Segment.source == self.name) & 
                   (schema.Segment.end > start) & 
                   (schema.Segment.start < end))
            .order_by(schema.Segment.sequence)
            .alias('ranked_intervals')
        )

        return [{'start': time_utils.time_to_json(start), 'end': time_utils.time_to_json(end)} for start, end in await self.server.db.all(
            db.select([
                db.func.min(ranked_intervals.c.start).label('start'),
                db.func.max(ranked_intervals.c.end).label('end')])
            .group_by(ranked_intervals.c.run)
            .order_by(ranked_intervals.c.run)
        )]

    async def get_n_segments_before(self, n, when):
        return list(reversed(
            await self.server.db.all(
                schema.Segment.query
                .where((schema.Segment.source == self.name) & (schema.Segment.end <= when))
                .order_by(schema.Segment.sequence.desc())
                .limit(n))
        ))

    async def get_segments_in_interval(self, start, end):
        return await self.server.db.all(
            schema.Segment.query
            .where((schema.Segment.source == self.name) & 
                   (schema.Segment.start < end) & 
                   (schema.Segment.end > start))
            .order_by(schema.Segment.sequence.asc()))

    async def get_newest_segment(self):
        return await self.server.db.first(
            schema.Segment.query.where(schema.Segment.source == self.name)
            .order_by(schema.Segment.sequence.desc())
            .limit(1)
        )

    async def get_max_known_sequence(self):
        last_segment = await self.get_newest_segment()
        if last_segment:
            self.logger.info(
                F('Max known seq: {#E:{}#}.', last_segment.sequence))
            return last_segment.sequence
        else:
            self.logger.info(F('No sequences recorded.'))
            return -1

    async def _pre_loop(self):
        # Delete everything inside the capture dir.
        self.logger.info(
            F('Cleaning up capture dir {#W:{}#}...', self.capture_dir))
        for entry in os.scandir(self.capture_dir):
            if entry.is_symlink() or entry.is_file():
                os.remove(entry.path)
            elif entry.is_dir():
                shutil.rmtree(entry.path)
        self.logger.info(
            F('Capture dir {#W:{}#} clean.', self.capture_dir))

        # Get the latest sequence number in the database (the first sequence
        # number we'd add to the database from this ffmpeg run).
        last_seq = await self.get_max_known_sequence()
        self._sequence_offset = last_seq + 1
        self.logger.info(
            F('Sequence offset: {#E:{}#}.', self._sequence_offset))

    async def adopt_new_segment(self, sequence, name, is_new_segment, time, duration, title):
        # The new filename is just based on the sequence number.
        new_name = "segment.%06d.ts" % (sequence,)

        # Adopt the file into the storage directory.
        source = os.path.join(self.capture_dir, name)
        if os.path.exists(source):
            destination = os.path.join(
                self.storage_dir, new_name)
            os.rename(source, destination)

        # Insert a new segment row.
        await schema.Segment.create(self.server.db,
                                    source=self.name,
                                    sequence=sequence,
                                    new_segment=is_new_segment,
                                    name=new_name,
                                    start=time,
                                    end=time + duration,
                                    title=title)

    @serializable_transaction()
    async def _garbage_collect_old_segments(self):
        if not self.retention_time:
            return

        # Calculate a protected interval depending on retention.
        now = datetime.datetime.now(tz=datetime.timezone.utc)
        retain_since = now - self.retention_time

        async def collect(start, end):
            # Ensure we yield.
            await self.sleep(datetime.timedelta())

            # Remove all the entries from the database and get their names.
            delete_query = schema.Segment.delete.where(
                schema.Segment.source == self.name)
            if start:
                delete_query = delete_query.where(
                    schema.Segment.start > start)
            if end:
                delete_query = delete_query.where(
                    schema.Segment.end < end)

            # Delete segments and find out which ones were deleted.
            segments_to_delete = await self.server.db.all(
                delete_query.returning(schema.Segment.name, schema.Segment.sequence))

            if segments_to_delete:
                # Ensure the next segment is marked as a discontinuity.
                last_erased_sequence = max(
                    segment.sequence for segment in segments_to_delete)
                await self.server.db.status(
                    schema.Segment.update.values(new_segment=True)
                    .where(
                        (schema.Segment.source == self.name) & (schema.Segment.sequence == last_erased_sequence + 1)
                    )
                )

                for segment in segments_to_delete:
                    # Ensure we yield.
                    await self.sleep(datetime.timedelta())
                    # Erase any existing file...
                    # this is obviously non-transactional...
                    path_to_remove = os.path.join(
                        self.storage_dir, segment.name)
                    if os.path.isfile(path_to_remove):
                        self.logger.info(
                            F('Erasing old segment: {#W:{}#}', path_to_remove))
                        os.remove(path_to_remove)

        prev_end = None

        # Add locks to the protected interval.
        for start, end in await self.server.db.all(
            db.select([schema.Event.start, schema.Event.end])
            .where(schema.EventSegmentLock.source == self.name)
            .where(schema.EventSegmentLock.event_id == schema.Event.id)
            .where(schema.Event.start < retain_since)
            .order_by(schema.Event.start)
        ):
            await collect(prev_end, start)
            prev_end = end
            if not prev_end:
                # The latest event isn't finished.
                return

        # After all events, delete until retain_since.
        await collect(prev_end, retain_since)

    @kill_server_on_exception
    async def _wait_for_stuckness(self):
        while True:
            await self._wait_for_stuckness_to_matter()
            await self.sleep(self.stuckness_delay)
            self.logger.error(
                F('Stuckness detected in capture dir: {#W:{}#}', self.capture_dir))
            await self._stuckness_detected()

    @kill_server_on_exception
    async def _watch_capture_dir(self):
        await self._watcher.setup(asyncio.get_running_loop())
        try:
            while True:
                event = await self._watcher.get_event()
                await self._file_changed(event.name)
        finally:
            try:
                self._watcher.close()
            except Exception:
                pass

    async def _file_changed(self, filename):
        self.logger.info(F('File changed: {#W:{}#}', filename))
        await self.cancel(self._stuck_future)
        self._stuck_future = self.spawn(self._wait_for_stuckness())

        if filename == PLAYLIST_FILENAME:
            # We might have new segments to add.
            await self._check_playlist()

    async def _start(self):
        await super()._start()
        os.makedirs(self.capture_dir, mode=0o750, exist_ok=True)
        os.makedirs(self.storage_dir, mode=0o750, exist_ok=True)

        self._stuck_future = self.spawn(self._wait_for_stuckness())
        self._watch_future = self.spawn(self._watch_capture_dir())

        await self._subprocess.start()

        await self._gc_task.start()

    async def _stop(self):
        await self._gc_task.stop()

        await self._subprocess.stop()

        await self.cancel(self._watch_future)
        await self.cancel(self._stuck_future)
        await super()._stop()

    def _make_app(self):
        app = super()._make_app()

        # Configure the resources handler; make the resources public. This
        # creates a mirror /resources/ path within our own subapp.
        self.server.resource_manager.setup(app)
        self.server.auth_manager.public_resources.add(app.router['resources'])

        # Set up jinja2 templates.
        # As a temporary hack for
        # https://github.com/aio-libs/aiohttp-jinja2/issues/193, don't add any
        # context processors here.
        aiohttp_jinja2.setup(
            app,
            loader=self.server.template_loader)

        self.resources.setup(self, app, self.server.auth_manager)

        return app

    @resources.register('/', name='root')
    @aiohttp_jinja2.template('hls_video.jinja2')
    async def _handle_root(self, request):
        timedelta = float(request.query.getone('timedelta', '0.0'))
        return dict(timedelta=timedelta)

    @resources.register(
        '/' + PLAYLIST_FILENAME,
        name='playlist', redirect_to_login=False)
    async def _handle_playlist(self, request):
        timedelta = datetime.timedelta(
            seconds=float(
                request.query.getone(
                    'timedelta', '0.0')))
        when = datetime.datetime.now(
            tz=datetime.timezone.utc) - timedelta

        segments = await self.get_n_segments_before(self.playback_list_size, when)
        if not segments:
            raise web.HTTPNotFound()

        playlist = m3u8.PlayList()
        playlist.from_segments(segments)

        last_segment = segments[-1]
        playlist_mtime = last_segment.end + timedelta

        return await web_utils.serve_text(request, playlist.text(), content_type='application/x-mpegURL',
                                          mtime=playlist_mtime)

    @resources.register(
        '/{segment:' + SEGMENT_FILENAME_REGEXP + '}',
        name='segment', redirect_to_login=False)
    async def _handle_segment(self, request):
        segment = request.match_info['segment']
        segment_path = os.path.join(self.storage_dir, segment)
        return await web_utils.serve_file(request, segment_path, content_type='video/MP2T',
                                          max_age=self.server.resource_manager.resource_cache_time)

    async def _create_segments_file(self, segments):
        fd, segment_file_path = tempfile.mkstemp(text=True)
        try:
            async with aiofiles.open(fd, mode='wt') as f:
                await f.write('ffconcat version 1.0\n')
                for segment in segments:
                    segment_path = os.path.join(self.storage_dir, segment.name)
                    await f.write('file \'{}\'\n'.format(segment_path))
                    await f.write('duration {}\n'.format((segment.end - segment.start).total_seconds()))

            return segment_file_path
        except Exception:
            os.remove(segment_file_path)
            raise

    async def _create_subtitles_file(self, segments, skip, length):
        fd, srt_file_path = tempfile.mkstemp(text=True)
        try:
            async with aiofiles.open(fd, mode='wt') as f:
                segment_offset = start_offset = datetime.timedelta(seconds=0)
                index = 1
                for segment in segments:
                    # Determine the offsets in the segment that should be
                    # covered.
                    if segment_offset < skip:
                        segment_start_offset = skip - segment_offset
                    else:
                        segment_start_offset = datetime.timedelta(seconds=0)

                    if segment_offset + segment.end - segment.start > skip + length:
                        segment_end_offset = skip + length - segment_offset
                    else:
                        segment_end_offset = segment.end - segment.start

                    start_time = segment.start + segment_start_offset
                    end_time = segment.start + segment_end_offset

                    while start_time < end_time:
                        next_time = min(
                            start_time.replace(
                                microsecond=0) + 
                            datetime.timedelta(
                                seconds=1),
                            end_time)
                        next_offset = start_offset + next_time - start_time

                        await f.write('{}\n'.format(index))
                        await f.write('{} --> {}\n'.format(time_utils.duration_to_srt(start_offset), time_utils.duration_to_srt(next_offset)))
                        await f.write('{} [{}]\n\n'.format(time_utils.time_to_human(start_time), self.name))

                        start_offset = next_offset
                        start_time = next_time
                        index += 1

                    segment_offset += segment.end - segment.start

            return srt_file_path
        except Exception:
            os.remove(srt_file_path)
            raise

    @resources.register(
        '/' + DOWNLOAD_FILENAME,
        name='download', redirect_to_login=False)
    async def _handle_download(self, request):
        start = datetime.datetime.fromtimestamp(float(
            request.query.getone(
                'start')), tz=datetime.timezone.utc)
        end = datetime.datetime.fromtimestamp(float(
            request.query.getone(
                'end')), tz=datetime.timezone.utc)

        # Find all the matching segments.
        segments = await self.get_segments_in_interval(start, end)

        # If any segment, check for head and tail cuttings.
        skip = datetime.timedelta(seconds=0)
        length = datetime.timedelta(seconds=0)
        for segment in segments:
            length += segment.end - segment.start

        if segments:
            if segments[0].start < start:
                skip = start - segments[0].start
                length -= skip
            if segments[-1].end > end:
                length -= segments[-1].end - end

        # Create a segment file.
        segment_file_path = await self._create_segments_file(segments)
        try:
            # Create a subtitle file.
            srt_file_path = await self._create_subtitles_file(segments, skip, length)
            try:
                # Prepare an ffmpeg commandline.
                command = self.command_line_start + [
                    '-ss', str(skip.total_seconds()),
                    '-f', 'concat',
                    '-safe', '0',
                    '-i', segment_file_path,
                    '-f', 'srt',
                    '-i', srt_file_path,
                    '-t', str(length.total_seconds()),
                    '-c', 'copy',
                    '-f', 'matroska',
                    '-'
                ]

                # Serve the output of the command.
                name = '{}-{}-{}.mkv'.format(self.name,
                                             time_utils.time_to_filename(start),
                                             time_utils.time_to_filename(end))
                return await web_utils.serve_command(request, self, *command, download=True, path=name, content_type='video/x-matroska')
            finally:
                os.remove(srt_file_path)
        finally:
            os.remove(segment_file_path)

    @serializable_transaction()
    async def _check_playlist(self):
        self.logger.info(
            F('Looking for raw playlist at {#W:{}#}...', self.raw_playlist_path))
        try:
            raw_playlist = m3u8.PlayList()
            async with aiofiles.open(self.raw_playlist_path, mode='rt', encoding='utf-8') as f:
                await raw_playlist.read(f)
            self.logger.info(
                F('Successfully read raw playlist at {#W:{}#}.', self.raw_playlist_path))

            # Adjust the playlist's media sequence and starting element.
            if raw_playlist.media_sequence == 0:
                raw_playlist.entries[0].is_new_segment = True
            raw_playlist.media_sequence += self._sequence_offset

            # Get the latest sequence number in the database.
            last_seq = await self.get_max_known_sequence()

            # Process the new sequences, adding them to the database.
            seq = raw_playlist.media_sequence - 1
            for entry in raw_playlist.entries:
                seq += 1
                if seq <= last_seq:
                    continue

                # Adopt the file into the storage directory and register it
                # in the database.
                await self.adopt_new_segment(
                    sequence=seq,
                    name=entry.name,
                    is_new_segment=entry.is_new_segment,
                    time=entry.time,
                    duration=entry.duration,
                    title=entry.title)
        except FileNotFoundError:
            self.logger.info(
                F('No raw playlist found at {#W:{}#}.', self.raw_playlist_path))
        except ValueError:
            self.logger.exception(
                F('Raw playlist at {#W:{}#} could not be parsed. Corrupt file?', self.raw_playlist_path))

    async def _wait_for_stuckness_to_matter(self):
        await self._subprocess.subprocess_running.wait()

    async def _stuckness_detected(self):
        await self.run_if_started(self._subprocess.terminate_subprocess)

    @property
    def raw_playlist_path(self):
        return os.path.join(self.capture_dir, PLAYLIST_FILENAME)

    @property
    def hls_segment_path(self):
        return os.path.join(
            self.capture_dir, '{}.%s.ts'.format(
                SEGMENT_FILENAME_PREFIX))

    @property
    def command_line_start(self):
        return [
            self.ffmpeg_binary,
            '-loglevel', self.ffmpeg_log_level,
            '-nostats',
        ]

    @property
    def command_line(self):
        return self.command_line_start + [
            '-fflags', 'nobuffer',
            '-probesize', '32',
            '-analyzeduration', '1000'
        ] + self.input_args + [
            '-codec', 'copy',
            '-f', 'hls',
            '-hls_flags', '+'.join(['program_date_time',
                                    'delete_segments']),
            '-use_localtime', '1',
            '-hls_segment_filename', self.hls_segment_path,
            '-hls_list_size', str(self.hls_list_size),
            '-hls_time', str(self.segment_time.total_seconds()),
            self.raw_playlist_path
        ]

    @property
    def input_args(self):
        raise NotImplementedError()

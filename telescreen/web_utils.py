import aiofiles
import datetime
import hashlib
import os

from aiohttp import web, hdrs

from collections import namedtuple
import inspect
import mimetypes
from telescreen import time_utils
from telescreen.serverobject import copy_stream


WebResource = namedtuple(
    'WebResource',
    'path, handler, methods, name, navbar_name, navbar_class, public, redirect_to_login, expect_handler, unbound')


WEB_RESOURCES_INSTANCE_ATTR_NAME = '__web_resources_instance__'


class WebResources(list):
    def __get__(self, instance, unused_owner=None):
        if not instance:
            # Class-level access.
            return self

        # Accessing an instance-level copy.
        per_instance_map = instance.__dict__.setdefault(
            WEB_RESOURCES_INSTANCE_ATTR_NAME, {})

        result = per_instance_map.get(id(self))
        if result:
            return result

        result = WebResources()
        result.extend(self)
        per_instance_map[id(self)] = result
        return result

    def add(self, path, handler, *args, **kwargs):
        self.register(path, *args, **kwargs)(handler)

    def register(
            self,
            path,
            methods=[
                hdrs.METH_GET,
                hdrs.METH_HEAD],
            name=None,
            navbar_name=None,
            navbar_class=None,
            public=False,
            redirect_to_login=True,
            expect_handler=None,
            unbound=False):
        def webresource_register_decorator(handler):
            self.append(
                WebResource(
                    path,
                    handler,
                    methods,
                    name,
                    navbar_name,
                    navbar_class,
                    public,
                    redirect_to_login,
                    expect_handler,
                    unbound))
            return handler

        return webresource_register_decorator

    def setup(self, server, app, auth_manager):
        router = app.router
        for web_resource in self:
            handler = web_resource.handler
            if server and not web_resource.unbound and not inspect.ismethod(
                    handler):
                # Bind the handler to the server.
                handler = handler.__get__(server)

            resource = router.add_resource(
                web_resource.path, name=web_resource.name)

            for method in web_resource.methods:
                resource.add_route(
                    method, handler,
                    expect_handler=web_resource.expect_handler)

            if web_resource.public:
                auth_manager.public_resources.add(resource)

            if not web_resource.redirect_to_login:
                auth_manager.unredirected_resources.add(resource)


async def process_caching(request, response, etag=None, mtime=None, max_age=None):
    # Datetimes in headers don't have fractional seconds.
    if mtime is not None:
        mtime = mtime.replace(microsecond=0)

    # Check ETag caching.
    if_none_match = request.headers.get(hdrs.IF_NONE_MATCH)
    if (if_none_match and etag and if_none_match == etag):
        raise web.HTTPNotModified()

    # Check If-Modified-Since caching.
    if_modified_since = request.headers.get(hdrs.IF_MODIFIED_SINCE)
    if (if_modified_since and mtime and time_utils.http_to_time(
            if_modified_since) >= mtime):
        raise web.HTTPNotModified()

    if max_age:
        response.headers[hdrs.CACHE_CONTROL] = 'max-age={:d}'.format(int(
            max_age.total_seconds()))
    if mtime:
        response.headers[hdrs.LAST_MODIFIED] = time_utils.time_to_http(
            mtime)
    if etag:
        response.headers[hdrs.ETAG] = '"{}"'.format(etag)


async def set_content_headers(request, response, content_type=None, encoding=None,
                              charset=None, download=False, filename=False, path=None):
    path = path or request.path

    if not content_type:
        content_type, guessed_encoding = mimetypes.guess_type(path)
        encoding = encoding or guessed_encoding

    if content_type:
        response.content_type = content_type
    if charset:
        response.charset = charset
    if encoding:
        response.headers[hdrs.CONTENT_ENCODING] = encoding
    if charset:
        response.charset = charset
    if download:
        filename = filename or os.path.basename(path)
        response.headers[hdrs.CONTENT_DISPOSITION] = 'attachment; filename="{}"'.format(
            filename)


async def serve_bytes(request, data, mtime=None, etag=None, max_age=None, **content_args):
    response = web.Response()

    if not etag:
        h = hashlib.sha256()
        h.update(data)
        etag = h.hexdigest()

    await process_caching(request, response, mtime=mtime, etag=etag, max_age=max_age)
    await set_content_headers(request, response, **content_args)

    response.body = data

    return response


async def serve_lines(request, lines, mtime=None, etag=None, max_age=None, charset='utf-8', **content_args):
    return await serve_text(
        request,
        ''.join(line + '\n' for line in lines),
        mtime=mtime, etag=etag, max_age=max_age, charset=charset,
        **content_args)


async def serve_text(request, text, mtime=None, etag=None, max_age=None, charset='utf-8', **content_args):
    response = web.Response(text=text)
    await process_caching(request, response, mtime=mtime, etag=etag, max_age=max_age)
    await set_content_headers(request, response, charset=charset, **content_args)
    return response


async def serve_json(request, json, mtime=None, etag=None, max_age=None, charset='utf-8', **content_args):
    response = web.json_response(json)
    await process_caching(request, response, mtime=mtime, etag=etag, max_age=max_age)
    await set_content_headers(request, response, charset=charset, **content_args)
    return response


async def serve_file(request, path=None, max_age=None, **content_args):
    try:
        size = os.path.getsize(path)
        mtime = datetime.datetime.fromtimestamp(
            os.path.getmtime(path), datetime.timezone.utc)
        response = web.StreamResponse()
        await process_caching(request, response, mtime=mtime,
                              max_age=max_age)
        await set_content_headers(request, response, path=path, **content_args)
        async with aiofiles.open(path, mode='rb') as f:
            response.content_length = size
            await response.prepare(request)
            await copy_stream(response, max_size=size)(f)
            await response.write_eof()
            return response
    except IOError:
        raise web.HTTPNotFound()


async def serve_command(request, process_spawner, *args, mtime=None, max_age=None, **content_args):
    response = web.StreamResponse()
    await process_caching(request, response, mtime=mtime,
                          max_age=max_age)
    await set_content_headers(request, response, **content_args)
    await response.prepare(request)

    proc = await process_spawner.spawn_subprocess(*args, stdout=copy_stream(response))
    
    exit_code = 0
    try:
        exit_code = await proc.exit_code()
    except Exception:
        await proc.terminate()
        raise
    
    if exit_code:
        raise web.HTTPInternalServerError()
    return response

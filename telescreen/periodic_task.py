import asyncio

from telescreen.serverobject import ServerObject, kill_server_on_exception


class PeriodicTask(ServerObject):
    def __init__(self, server, name, period, callback):
        super().__init__(name, server)
        self.period = period
        self.callback = callback
        self._awake_future = None

    async def awaken(self):
        async with self._lock:
            if self._awake_future:
                await self.cancel(self._awake_future)

    async def _cancellable_sleep(self, wait_period):
        try:
            await self.sleep(wait_period)
        except asyncio.CancelledError:
            pass

    @kill_server_on_exception
    async def _loop(self):
        while True:
            wait_period = (await self.callback()) or self.period
            async with self._lock:
                awake_future = self._awake_future = self.spawn(
                    self._cancellable_sleep(wait_period))

            await awake_future

    async def _start(self):
        await super()._start()
        self._loop_future = self.spawn(self._loop())

    async def _stop(self):
        await self.cancel(self._loop_future)
        if self._awake_future:
            await self.cancel(self._awake_future)
        await super()._stop()

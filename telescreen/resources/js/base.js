function showOverlay(text, type, link_name, link_url, detail) {
	var message_container = document.getElementById("top_message");
	while (message_container.firstChild) {
		message_container.firstChild.remove();
	}
	
	var p = document.createElement("p");
	p.classList.add(type);
	
	p.appendChild(document.createTextNode(text));
	
	if (link_url && link_name) {
		p.appendChild(document.createTextNode(" "));
		
		a = document.createElement("a");
		a.href = link_url;
		a.text = link_name;
		p.appendChild(a);
	}
	
	message_container.appendChild(p);
	
	if (detail) {
		p = document.createElement("p");
		p.classList.add('detail');
		p.appendChild(document.createTextNode(detail));
		message_container.appendChild(p);
	}
	
	message_container.style.visibility = "visible";
}

function clearOverlay() {
	var message_container = document.getElementById("top_message");
	message_container.style.visibility = "hidden";
	while (message_container.firstChild) {
		message_container.firstChild.remove();
	}
}

function check_server_connectivity() {
	function server_error(error) {
		showOverlay(
			'Connection to the server lost...',
			'warning',
			'reload',
			'javascript:location.reload();',
			error
		);						
	}
	
	fetch('/ping', { credentials: 'same-origin' }).then(function(response){
		if (response.ok) {
			clearOverlay();
			window.setTimeout(check_server_connectivity, 5000);
		}
		else if (response.status == 401) {
			location.reload();
		}
		else {
			server_error('HTTP ' + response.status + ' ' + response.statusText)
			window.setTimeout(check_server_connectivity, 5000);
		}
	}).catch(function(error) {
		server_error(error)
		window.setTimeout(check_server_connectivity, 5000);		
	});
}

function fetch_periodically(path, period_ms, on_success, on_error) {
	function maybe_schedule(path) {
		if (path == null) {
			return;
		}
		
		window.setTimeout(function() {loop(path);}, period_ms);
	}
	
	function loop(path) {		
		fetch(path, { credentials: 'same-origin' }).then(function(response){
			if (response.ok) {
				return response;
				path = on_success(response);
				maybe_schedule(path);
			}
			else {
				throw new TypeError(response.status);
			}
		}).then(function(response){
			return response.json();
		}).then(function(jsondata){
			path = on_success(jsondata);
			maybe_schedule(path);
		}).catch(function(error) {
			if (on_error) {
				path = on_error(error);
			}
			maybe_schedule(path);
		});
	}
	
	window.setTimeout(function() {loop(path);}, 0);
}
var json_path = '/timeline.json';
var timeline_data = {
    up_to : null,
    continue_at : null,
    cameras : {}
}
var timeline_groups = null;
var timeline_items = null;
var timeline_ctrl = null;
var period_sequence = 0;
var last_full_scan = null;

function update_timeline(data) {
    let now = new Date();
    let update_datasets = false;
    if (last_full_scan == null) {
        timeline_items = new vis.DataSet();
        if (timeline_groups == null) {
            timeline_groups = new vis.DataSet();
        }
        update_datasets = true;
    }

    // Iterate on all the cameras.
    let camera_names = Object.keys(data.cameras);
    for (let i = 0; i < camera_names.length; i++) {
        let camera_name = camera_names[i];
        let group = null;
        let group_name = 'camera_' + camera_name;

        // Create the camera structure and group if it wasn't found.
        if (!(camera_name in timeline_data.cameras)) {
            timeline_data.cameras[camera_name] = {};
            group = {
                id : group_name,
                content : camera_name,
                className : 'timeline-camera-group'
            };
            timeline_groups.update(group);
        } else {
            group = timeline_groups.get(group_name);
        }

        // Iterate on all camera periods.
        let periods = data.cameras[camera_name];
        for (let j = 0; j < periods.length; j++) {
            let period = periods[j];
            let new_start = period.start;
            let new_end = period.end;
            let found = false;
            // Find the first candidate for merging.
            // We should not have overlaps with more than one period.
            let old_periods = Object
                    .entries(timeline_data.cameras[camera_name]);
            for (let k = 0; k < old_periods.length; k++) {
                let period_entry = old_periods[k];
                let key = period_entry[0];
                let start = period_entry[1].start;
                let end = period_entry[1].end;

                if (new_start <= end && new_end >= start) {
                    // There's an overlap; merge and update.
                    new_start = Math.min(start, new_start);
                    new_end = Math.max(end, new_end);

                    timeline_data.cameras[camera_name][key] = {
                        start : new_start,
                        end : new_end
                    };

                    timeline_items.update({
                        id : key,
                        group : group_name,
                        start : new Date(new_start * 1000),
                        end : new Date(new_end * 1000),
                        className : 'timeline-camera-item',
                        type : 'background'
                    });

                    found = true;
                    break;
                }
            }

            if (!found) {
                // This a new entry. Assign an ID and put it in.
                let key = 'segment_' + period_sequence++;

                timeline_data.cameras[camera_name][key] = {
                    start : new_start,
                    end : new_end
                };

                timeline_items.add({
                    id : key,
                    group : group_name,
                    start : new Date(new_start * 1000),
                    end : new Date(new_end * 1000),
                    className : 'timeline-camera-item',
                    type : 'background'
                });
            }
        }
    }

    // Iterate on all the events.
    let event_names = Object.keys(data.events);
    for (let i = 0; i < event_names.length; i++) {
        let event_name = event_names[i];
        let group_name = 'event_' + event_name;
        let signals_group_name = 'signals_' + event_name;

        // Create the groups if they are missing.
        if (!timeline_groups.get(group_name)) {
            let group = {
                id : group_name,
                content : event_name,
                nestedGroups : [ signals_group_name ],
                showNested : false,
                className : 'timeline-event-group'
            };
            timeline_groups.add(group);
        }
        if (!timeline_groups.get(signals_group_name)) {
            let group = {
                id : signals_group_name,
                content : 'signals',
                className : 'timeline-event-signals-group'
            };
            timeline_groups.add(group);
        }

        // Iterate on all events.
        let event_entries = Object.entries(data.events[event_name]);
        for (let j = 0; j < event_entries.length; j++) {
            let event_entry = event_entries[j];
            let key = 'event_' + event_entry[0];
            let start = event_entry[1].start;
            let end = event_entry[1].end;

            // Find the item in the DataSet.
            class_name = 'timeline-event-item';
            if (end == null) {
                class_name += ' pending';
                end = data.up_to;
            }
            let item = {
                id : key,
                group : group_name,
                start : new Date(start * 1000),
                end : new Date(end * 1000),
                className : class_name,
                content : event_name,
                type : 'range'
            };
            if (timeline_items.get(key)) {
                // Update the existing one.
                timeline_items.update(item);
            } else {
                // Create a new one.
                timeline_items.add(item);
            }

            // Iterate on all signals.
            let signal_entries = Object.entries(event_entry[1].signals);
            for (let k = 0; k < signal_entries.length; k++) {
                let signal_entry = signal_entries[k];
                let key = 'signal_' + signal_entry[0];
                let start = signal_entry[1].start;
                let end = signal_entry[1].end;
                let signal_name = signal_entry[1].name;
                let subgroup = group_name + '/' + signal_name;

                // Find the item in the DataSet.
                class_name = 'timeline-signal-item';
                if (end == null) {
                    class_name += ' pending';
                    end = data.up_to;
                }
                let item = {
                    id : key,
                    group : signals_group_name,
                    subgroup : subgroup,
                    start : new Date(start * 1000),
                    end : new Date(end * 1000),
                    className : class_name,
                    content : signal_name,
                    type : 'range'
                };
                if (timeline_items.get(key)) {
                    // Update the existing one.
                    timeline_items.update(item);
                } else {
                    // Create a new one.
                    timeline_items.add(item);
                }
            }
        }
    }

    var container = document.getElementById('timeline');

    if (timeline_ctrl == null) {
        let now = new Date();
        let since = new Date(now.getTime() - 86400000);
        options = {
            start : since,
            end : now,
            stack : false,
            verticalScroll : true,
            maxHeight : "33vh",
            stackSubgroups : true,
            rollingMode : {
                follow : true,
                offset : 1.0
            }
        };
        timeline_ctrl = new vis.Timeline(container, timeline_items,
                timeline_groups, options);

        // Force a redraw when expanding groups.
        // Workaround https://github.com/almende/vis/issues/4030
        timeline_ctrl.on('click', function (properties) {
            if (properties.what == 'group-label') {
                timeline_ctrl.redraw();
            }
        });
        last_full_scan = now;
    } else if (update_datasets) {
        timeline_ctrl.setItems(timeline_items);
        last_full_scan = now;
    }

    timeline_data.up_to = data.up_to;
    timeline_data.continue_at = data.continue_at;
    container.classList.remove('loading');

    // Hard-code full scans to each 5 minutes.
    if (now - last_full_scan > 300000) {
        last_full_scan = null;
        timeline_data = {
            up_to : null,
            continue_at : null,
            cameras : {}
        }
    }
}

fetch_periodically(json_path, 5000, function (jsondata) {
    update_timeline(jsondata);
    var path = json_path;
    if (timeline_data.continue_at != null) {
        path = path + '?start=' + timeline_data.continue_at;
    }
    return path;
});
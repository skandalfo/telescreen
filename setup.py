#!/usr/bin/env python3
from setuptools import setup, find_packages

setup(
    name='TeleScreen',
    version='0.1',
    install_requires=[
        'aiocontextvars>=0.2.2',
        'aiofiles>=0.5.0',
        'aiohttp>=3.6.2,<4.0.0',
        'aiohttp_debugtoolbar>=0.6.0',
        'aiohttp_jinja2>=1.2.0',
        'aiohttp_session[secure]>=2.9.0',
        'aiohttp_security[session]>=0.4.0',
        'aionotify>=0.2.0',
        'aiosmtplib>=1.1.4',
        'aiosmtpd>=1.2',
        'cryptography>=3.1.1',
        'uvloop>=0.14.0',
        'gino>=1.0.1',
    ],
    packages=find_packages(),
    include_package_data=True,
    entry_points={
        'console_scripts': [
            'telescreen=telescreen:main',
        ],
        'telescreen.server.TeleScreenServer': [
            'TeleScreenServer=telescreen.server:TeleScreenServer',
        ],
        'telescreen.camera.Camera': [
            'RTSPCamera=telescreen.rtsp_camera:RTSPCamera',
        ],
        'telescreen.notification.Notification': [
            'LoggingNotification=telescreen.notification:LoggingNotification',
            'EmailNotification=telescreen.notification:EmailNotification',
        ],
        'telescreen.event.Event': [
            'Event=telescreen.event:Event',
        ],
        'telescreen.receiver.Receiver': [
            'EmailReceiver=telescreen.receiver:EmailReceiver',
            'MotionReceiver=telescreen.motion:MotionReceiver',
        ],
        'telescreen.Template': [
            'base=telescreen',
        ],
        'telescreen.Resource': [
            'base=telescreen',
        ],
    })
